﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchScaler : MonoBehaviour 
{
    float zoomSpeed = 0.2f;

    void Update()
    {
        if (Input.touchCount == 2)
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            transform.localScale -= deltaMagnitudeDiff * zoomSpeed * Vector3.one * Time.deltaTime;
            var x = transform.localScale.x;
            x = Mathf.Clamp(x, 0.2f, 3);
            transform.localScale = Vector3.one * x;
        }
    }
}
