﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchRotator : MonoBehaviour 
{
    private Vector3 prevPos = Vector3.zero, 
                    rotAxis = Vector3.up;
    public float    rotSensivity = 7, 
                    targetRotSpeed = 0, 
                    currentRotSpeed = 0, 
                    inertion = 1f;

    public bool lockX, lockZ;

	// Use this for initialization
	void Start () 
    {
        
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.touchCount == 2)
            return;
        
        if (Input.GetMouseButton(0))
        {
            var pos = Input.mousePosition + Vector3.forward;
            if (prevPos != Vector3.zero)
            {
                var worldDelta = Camera.main.ScreenToWorldPoint(pos);
                worldDelta -= Camera.main.ScreenToWorldPoint(prevPos);

                rotAxis = -Vector3.Cross(Camera.main.transform.forward, worldDelta);
                rotAxis = transform.InverseTransformDirection(rotAxis);
                targetRotSpeed = rotAxis.magnitude * rotSensivity * 10000 * Time.deltaTime; //max is about 14000 with fast dragging

                if (lockX) rotAxis.x = 0;
                if (lockZ) rotAxis.z = 0;
            }
            prevPos = pos;
        }
        else
        {
            prevPos = Vector3.zero;
            targetRotSpeed = 0;
        }

        // NEXT STRING IS EQUAL TO THIS: currentRotSpeed = currentRotSpeed + (targetRotSpeed - currentRotSpeed) * 0.001f;
        float lerp = Time.deltaTime / inertion;
        lerp *= targetRotSpeed > currentRotSpeed ? 4f : 1f;
        currentRotSpeed = Mathf.Lerp(currentRotSpeed, targetRotSpeed, lerp);

        // epsilon stop
        if (Mathf.Abs(targetRotSpeed - currentRotSpeed) < 0.1f)
        {
            currentRotSpeed = targetRotSpeed;
        }

        transform.Rotate(rotAxis, currentRotSpeed * Time.fixedDeltaTime);
	}
}
