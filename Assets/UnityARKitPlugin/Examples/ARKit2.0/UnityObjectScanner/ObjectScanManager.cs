﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;
using System;
using UnityEngine.UI;
using System.IO;
using System.Linq;

public class ObjectScanManager : MonoBehaviour
{
    static UnityARSessionNativeInterface session
    {
        get { return UnityARSessionNativeInterface.GetARSessionNativeInterface(); }
    }

    public static ObjectScanManager shared;

	[SerializeField]
	public ObjectScanSessionManager m_ARSessionManager;

	[SerializeField]
	public Text listOfObjects;

	int objIndex = 0;
	List<ARReferenceObject> scannedObjects;
	//bool detectionMode = false;

	private PickBoundingBox pickBoundingBox;

    [SerializeField]
    Vector3[] pointsCache = { };
    ARObjectAdapterData lastObject = new ARObjectAdapterData();

    public List<Vector3> scannedPoints = new List<Vector3>();

	private void Awake()
	{
        shared = this;
        scannedObjects = new List<ARReferenceObject>();
        pickBoundingBox = transform.GetChild(0).GetComponent<PickBoundingBox>();
	}

	void Start()
	{
        UnityARSessionNativeInterface.ARFrameUpdatedEvent += ARFrameUpdated;
        UnityARSessionNativeInterface.ARAnchorAddedEvent += ARAnchorAdded;
	}

	void OnDestroy()
	{
        UnityARSessionNativeInterface.ARFrameUpdatedEvent -= ARFrameUpdated;
        UnityARSessionNativeInterface.ARAnchorAddedEvent -= ARAnchorAdded;
    }

	private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.S))
        {
            print("Simulating plane anchor");
            FindObjectOfType<ScanUIController>().ARKitIsReadyToScan();
        }
#endif
	}

	public void CreateReferenceObject(Action<ARObjectAdapterData> callback)
	{
		CreateReferenceObject (pickBoundingBox.transform, pickBoundingBox.bounds.center-pickBoundingBox.transform.position, pickBoundingBox.bounds.size, callback);
	}

	public void CreateReferenceObject(Transform objectTransform, Vector3 center, Vector3 extent, Action<ARObjectAdapterData> callback)
	{
        if (lastObject.Points.Any()) {
            var boundingBoxBottomCenter = pickBoundingBox.bounds.center - Vector3.up * pickBoundingBox.bounds.size.y / 2;
            callback(new ARObjectAdapterData
            {
                Points = lastObject.Points.Where(pickBoundingBox.bounds.Contains).Select(x => x - boundingBoxBottomCenter).ToArray()
            });
        }
        return;

        session.ExtractReferenceObjectAsync (objectTransform, center, extent, (ARReferenceObject referenceObject) => {
			if (referenceObject != null) {
                callback(new ARObjectAdapterData(referenceObject));
			} else {
				Debug.Log ("Failed to create ARReferenceObject.");
			}
		});
	}

	void UpdateList()
	{
		string members = "";
		foreach (ARReferenceObject arro in scannedObjects) {
            members += String.Format("{0} ({1}),", arro.name, arro.pointCloud.Count);
		}
    }

    void UpdatePointsCountText()
    {
        FindObjectOfType<ScanUIController>().ShowPointsCount();
    }

    public void ARFrameUpdated(UnityARCamera camera)
    {
        if (camera.pointCloud != null)
        {
            lastObject.Points = camera.pointCloud.Points;
            lastObject.Identifiers = camera.pointCloud.Identifiers;
        }
    }

    public void ARAnchorAdded(ARPlaneAnchor planeAnchor)
    {
        FindObjectOfType<ScanUIController>().ARKitIsReadyToScan();
    }

    public void LockCube(bool lockCube)
    {
        pickBoundingBox.enabled = !lockCube;
    }

    public void ShowCube(bool showCube)
    {
        pickBoundingBox.gameObject.SetActive(showCube);
    }

	public void AddPoints()
	{
#if UNITY_EDITOR
        var pts = ARObjectAdapter.pointsFromFile("allPoints_objScan.json");
        scannedPoints.AddRange(pts);
        UpdatePointsCountText();
#else
        CreateReferenceObject((ARObjectAdapterData obj) =>
        {
            Debug.Log("obj == nil ? " + obj==null);
            scannedPoints.AddRange(obj.Points);
            UpdatePointsCountText();
        });
#endif
	}

    public void StartScanning()
    {
        m_ARSessionManager.StartObjectScanningSession();
    }

    public void StartDetecting()
	{
		//create a set out of the scanned objects
		IntPtr ptrReferenceObjectsSet = session.CreateNativeReferenceObjectsSet(scannedObjects);

		//restart session without resetting tracking 
		var config = m_ARSessionManager.sessionConfiguration;

		//use object set from above to detect objects
		config.dynamicReferenceObjectsPtr = ptrReferenceObjectsSet;

		//Debug.Log("Restarting session without resetting tracking");
		session.RunWithConfigAndOptions(config, UnityARSessionRunOption.ARSessionRunOptionRemoveExistingAnchors | UnityARSessionRunOption.ARSessionRunOptionResetTracking);

	}

    public void PauseSession()
    {
        m_ARSessionManager.PauseSession();
    }

	public void SaveScannedObjects()
	{
		if (scannedObjects.Count == 0)
			return;

		string pathToSaveTo = Path.Combine(Application.persistentDataPath, "ARReferenceObjects");

		if (!Directory.Exists (pathToSaveTo)) 
		{
			Directory.CreateDirectory (pathToSaveTo);
		}

		foreach (ARReferenceObject arro in scannedObjects) 
		{
			string fullPath = Path.Combine (pathToSaveTo, arro.name + ".arobject");
			arro.Save (fullPath);

            new ARObjectAdapterData(arro).Save(arro.name + ".json");
		}
	}
}
