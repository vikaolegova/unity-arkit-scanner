﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.iOS;

public class GenerateAnchorBoard : MonoBehaviour {

    public ARReferenceImagesSet images;
    public GameObject singlePrefab;

    private Dictionary<string, GameObject> imagesGOs = new Dictionary<string, GameObject>();

	void Start () {
        foreach (var img in images.referenceImages)
        {
            imagesGOs[img.imageName] = Instantiate(singlePrefab);
            imagesGOs[img.imageName].SetActive(false);
        }

        Subscribe();
	}

    public void Subscribe()
    {
        UnityARSessionNativeInterface.ARImageAnchorAddedEvent += AddImageAnchor;
        UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent += UpdateImageAnchor;
        UnityARSessionNativeInterface.ARImageAnchorRemovedEvent += RemoveImageAnchor;
    }

    public void Unsubscribe()
    {
        UnityARSessionNativeInterface.ARImageAnchorAddedEvent -= AddImageAnchor;
        UnityARSessionNativeInterface.ARImageAnchorUpdatedEvent -= UpdateImageAnchor;
        UnityARSessionNativeInterface.ARImageAnchorRemovedEvent -= RemoveImageAnchor;

        foreach (var go in imagesGOs.Values) {
            go.SetActive(false);
        }
    }

    void OnDestroy()
    {
        Unsubscribe();
    }

    void AddImageAnchor(ARImageAnchor arImageAnchor)
    {
        Debug.LogFormat("image anchor added[{0}] : tracked => {1}", arImageAnchor.identifier, arImageAnchor.isTracked);

        UpdateImageAnchor(arImageAnchor);
    }

    void UpdateImageAnchor(ARImageAnchor arImageAnchor)
    {
        Debug.LogFormat("image anchor updated[{0}] : \ntracked => {1} \nreferenceImageName: {2}", arImageAnchor.identifier, arImageAnchor.isTracked, arImageAnchor.referenceImageName);

        if (!imagesGOs.ContainsKey(arImageAnchor.referenceImageName)) {
            return;
        }
        var go = imagesGOs[arImageAnchor.referenceImageName];
        if (go)
        {
            if (arImageAnchor.isTracked)
            {
                Vector3 position = UnityARMatrixOps.GetPosition(arImageAnchor.transform);
                Quaternion rotation = UnityARMatrixOps.GetRotation(arImageAnchor.transform);

                go.transform.position = position;
                go.transform.rotation = rotation;

                if (!go.activeSelf)
                    go.SetActive(true);
            } else if (go.activeSelf) {
                go.SetActive(false);
            }
        }
    }

    void RemoveImageAnchor(ARImageAnchor arImageAnchor)
    {
        Debug.LogFormat("image anchor removed[{0}] : tracked => {1}", arImageAnchor.identifier, arImageAnchor.isTracked);

        var go = imagesGOs[arImageAnchor.referenceImageName];
        if (go)
            go.SetActive(false);

    }

}
