﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour 
{
    [System.Serializable]
    public class JSONWrapper
    {
        public List<Vector3> points;
    }

    public static UIController shared { get; private set; }

    private const string pressStart = "Сгенерируйте точки и нажмите \"Старт\"";

    public Text[] texts;
    public Text currentMsg;
    public Button expand;
    public Text expandLabel;
    public Text stopResume;
    public Toggle byStep;
    public Toggle drawMeshes;
    public Slider speedSlider;
    public Dropdown pointsTypeDropdown;

    public Vector2 delay = Vector2.up;

    public Slider countSlider;
    public InputField countInput;
    public InputField fileInput;

    public Text generateLabel;

    private bool started = false;

    public static Vector3 GeneratePoint(int type)
    {
        var size = 5f;

        switch (type)
        {
            case 0:
                //sphere
                return Random.insideUnitSphere * size * 2;

            case 1:
                //hull
                return Random.onUnitSphere * size * 2;

            case 2:
                //cube
                return new Vector3(Random.Range(-size, size), Random.Range(-size, size), Random.Range(-size, size));
        }

        return Vector3.zero;
    }

    public void OnGenerateClicked()
    {
        DelaunayBuilder.shared.Reset(true);
        OnMessage(pressStart);

        started = false;

        var t = pointsTypeDropdown.value;
        if (t < 3)
        {
            for (int i = 0; i < countSlider.value; i++)
                DelaunayBuilder.shared.AddPoint(GeneratePoint(t));
        }
        else
        {
            try
            {
                var text = System.IO.File.ReadAllText(fileInput.text.Trim());

                var arr = JsonUtility.FromJson<JSONWrapper>(text).points;

                countInput.text = arr.Count.ToString();
                foreach (var p in arr)
                {
                    DelaunayBuilder.shared.AddPoint(p);
                }

                fileInput.text = "Загружено " + arr.Count + " точек.";
            }
            catch (System.Exception ex)
            {
                Debug.Log(ex.Message);

                fileInput.text = ex.Message;
            }
        }
    }

    public void OnStartClicked()
    {
        DelaunayBuilder.shared.Reset(false);
        OnMessage(pressStart);

        started = false;
    }

    public void OnStopResumeClicked()
    {
        if (Tetrahedron.realPoints.Count < 5)
            return;

        DelaunayBuilder.shared.insertingEnabled = !DelaunayBuilder.shared.insertingEnabled || byStep.isOn;

        if(byStep.isOn)
        {
            DelaunayBuilder.shared.nextStep = true;
        }

        started = true;
    }

    public void OnExpandClicked()
    {
        if ((DelaunayBuilder.shared.insertingEnabled && !byStep.isOn && !DelaunayBuilder.shared.Finished) || !started)
            return;

        DelaunayBuilder.shared.expand = !DelaunayBuilder.shared.expand;
    }

    public void OnMessage(string msg)
    {
        currentMsg.text = msg;
    }

    public void OnByStepChanged(bool value)
    {
        if (!value)
        {
            DelaunayBuilder.shared.insertingEnabled = false;
        }
    }

	void Awake()
	{
        shared = this;

        var arr = new List<string>();
        for (int i = 0; i < 20; i++)
            arr.Add(JsonUtility.ToJson(GeneratePoint(1)));
        
        //Debug.Log(arr.JoinString());
	}

    void UpdateUILabels()
    {
        if (DelaunayBuilder.shared.Finished)
            stopResume.text = "Готово";
        else
            stopResume.text = DelaunayBuilder.shared.insertingEnabled && !byStep.isOn ? "Стоп" : started ? "Продолжить" : "Старт";
        expandLabel.text = DelaunayBuilder.shared.expand ? "Сдвинуть" : "Раздвинуть";
    }

    void Start () 
    {
        byStep.onValueChanged.AddListener((x) => OnByStepChanged(x));
        drawMeshes.onValueChanged.AddListener(x =>
        {
            DelaunayBuilder.shared.instantDraw = x;
            if (x)
                DelaunayBuilder.shared.RandomizeColors();
        });

        countSlider.onValueChanged.AddListener((x) =>
        {
            countInput.text = x.ToString();
        });

        countInput.onEndEdit.AddListener((x) =>
        {
            var y = Mathf.Clamp(int.Parse(x), (int)countSlider.minValue, (int)countSlider.maxValue);
            countInput.text = y.ToString();
            countSlider.value = y;
        });

        pointsTypeDropdown.onValueChanged.AddListener(x =>
        {
            countSlider.transform.parent.gameObject.SetActive(x < 3);
            fileInput.gameObject.SetActive(x == 3);
            generateLabel.text = x == 3 ? "Загрузить из файла" : "Сгенерировать точки";
            if (x==3)
            {
                fileInput.text = "";
            }
        });
        pointsTypeDropdown.onValueChanged.Invoke(pointsTypeDropdown.value);

        OnMessage(pressStart);



        DelaunayBuilder.shared.instantDraw = drawMeshes.isOn;
	}
	
    void Update () {
        texts[0].text = DelaunayBuilder.shared.insertedPoints.ToString();
        texts[1].text = Tetrahedron.tetrahedrons.Count.ToString();
        texts[2].text = DelaunayBuilder.shared.flips14.ToString();
        texts[3].text = DelaunayBuilder.shared.flips26.ToString();
        texts[4].text = DelaunayBuilder.shared.flips23.ToString();
        texts[5].text = DelaunayBuilder.shared.flips32.ToString();
        texts[6].text = DelaunayBuilder.shared.flips44.ToString();
        texts[7].text = DelaunayBuilder.shared.flipsM2M.ToString();

        UpdateUILabels();

        DelaunayBuilder.shared.instantDraw = drawMeshes.isOn;
        DelaunayBuilder.shared.delays = byStep.isOn;
        DelaunayBuilder.shared.stepDelay = (1 - speedSlider.normalizedValue) * (delay.y - delay.x) + delay.x;

        if (!byStep.isOn)
        {
            DelaunayBuilder.shared.nextStep = true;
        }
	}
}
