﻿using UnityEngine;
using System.Collections;

public class MathHelper
{
    public static double epsilon = 1.0E-10;
    public static double planeDistEpsilon = 0.0001;

    public static bool NearlyZero(float x)
    {
        return -epsilon < x && x < epsilon;
    }

    public static bool NearlyZero(double x)
    {
        return -epsilon < x && x < epsilon;
    }

    public static float DistanceToPlane(Vector3 a, Vector3 b, Vector3 c, Vector3 p)
    {
        var plane = new Plane(a, b, c);
        return Mathf.Abs(plane.GetDistanceToPoint(p));
    }

    public static bool AreCoplanar(Vector3 a, Vector3 b, Vector3 c, Vector3 d)
    {
        var dist = DistanceToPlane(a, b, c, d);
        //Debug.Log("dist: " + dist);
        return dist < planeDistEpsilon;
    }

    /// <summary>
    /// Checks if a point p is inside the sphere formed by a, b, c, d points.
    /// 
    /// Observe that to obtain these results, the points a, b, c and d
    /// must be ordered such that Orient (a, b, c, d) returns a positive value.
    /// </summary>
    /// <returns>
    /// a positive value if p is inside the sphere;
    /// a negative if p is outside;
    /// 0 if p is directly on the sphere.
    /// </returns>
    public static double InSphere(Vector3 a, Vector3 b, Vector3 c, Vector3 d, Vector3 p)
    {
        var matrix = new double[,]{
                {a.x, a.y, a.z, a.sqrMagnitude, 1f},
                {b.x, b.y, b.z, b.sqrMagnitude, 1f},
                {c.x, c.y, c.z, c.sqrMagnitude, 1f},
                {d.x, d.y, d.z, d.sqrMagnitude, 1f},
                {p.x, p.y, p.z, p.sqrMagnitude, 1f}
            };

        return MatrixMath.Determinant(matrix) * Orient(a, b, c, d);
    }

    /// <summary>
    /// Orient returns a positive value when the point p is above the plane defined by a, b and c; 
    /// a negative value if p is under the plane; 
    /// 0 if p is directly on the plane. 
    /// 
    /// Orient is consistent with the left-hand rule: 
    /// when the ordering of a, b and c follows the direction of
    /// rotation of the curled fingers of the left hand, 
    /// then the thumb points towards the positive side (the above side of the plane)
    /// </summary>
    public static double Orient(Vector3 a, Vector3 b, Vector3 c, Vector3 p)
    {
        var matrix = new double[,]{
                {a.x, a.y, a.z, 1f},
                {b.x, b.y, b.z, 1f},
                {c.x, c.y, c.z, 1f},
                {p.x, p.y, p.z, 1f}
            };

        return MatrixMath.Determinant(matrix);
    }
}
