﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class MeshPointsExtractor : MonoBehaviour 
{
    private List<Vector3> points;
    public MeshFilter[] meshes;
    public float scale = 10;

    public bool triangulate = false;

	void Start () 
    {
        points = new List<Vector3>();
        foreach(var m in meshes)
        {
            points.AddRange(m.mesh.vertices.Distinct().Select(x => m.transform.TransformPoint(x) * scale));
        }

        //points = mesh.vertices.Distinct().Select(x=>x*scale).ToList();
        Debug.Log(points.Count + " points extracted");
	}
	
	void Update () 
    {
        if (triangulate)
        {
            triangulate = false;
            foreach (var p in points)
                DelaunayBuilder.shared.AddPoint(p);
        }
    }
}

public static class CommonUtil
{
    public static string JoinString<T>(this IEnumerable<T> list)
    {
        return "[ " + string.Join(", ", list.Select(x => x.ToString()).ToArray()) + " ]";
    }
}