﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisiblePoint : System.IDisposable
{
    public static implicit operator Vector3(VisiblePoint v)
    {
        return v.point;
    }

    public static implicit operator VisiblePoint(Vector3 v)
    {
        return new VisiblePoint(v);
    }

    private float _size = 1;
    private MeshRenderer renderer;

    public Vector3 point { get; private set; }
    public GameObject gameObject { get; private set; }

    public float size
    {
        get
        {
            return _size;
        }
        set
        {
            _size = value;
            gameObject.transform.localScale = _size * Vector3.one;
        }
    }


    public Color Color
    {
        get
        {
            return renderer.material.color;
        }
        set
        {
            renderer.material.color = value;
            renderer.enabled = value.a > 0;
        }
    }

    public VisiblePoint(Vector3 v, Color color, float size = 1f)
    {
        point = v;

        gameObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        renderer = gameObject.GetComponent<MeshRenderer>();
        var col = gameObject.GetComponent<Collider>();
        if (col) col.enabled = false;
        gameObject.transform.position = point;

        this.size = size;
        Color = color;
    }

    public VisiblePoint(Vector3 v) : this(v, Color.clear)
    {
        
    }

    //~VisiblePoint()
    //{
    //    Object.DestroyObject(gameObject, 0.1f);
    //}

    #region IDisposable Support
    private bool disposedValue = false; // To detect redundant calls

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                Object.Destroy(gameObject);
            }

            disposedValue = true;
        }
    }

    // This code added to correctly implement the disposable pattern.
    public void Dispose()
    {
        Dispose(true);
    }
    #endregion
}
