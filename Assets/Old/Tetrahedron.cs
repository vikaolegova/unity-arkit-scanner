﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class Tetrahedron : System.IDisposable
{
    public static HashSet<Tetrahedron> tetrahedrons = new HashSet<Tetrahedron>();
    public static Dictionary<int, HashSet<Tetrahedron>> tetrahedronsForPoint = new Dictionary<int, HashSet<Tetrahedron>>();

    public static List<VisiblePoint> realPoints = new List<VisiblePoint>();
    public static Material material;
    public static Transform parentTransform;

    private static int nextId = 0;

    public int Id { get; private set; }
    public bool bad = false;

    public override string ToString()
    {
        if (disposedValue)
            return Id + " DISPOSED";
        return Id + "_[ " + string.Join(", ", Points.Select(x => x.ToString()).ToArray()) + " ]";
    }

    public override int GetHashCode()
    {
        return Id;
    }

    private List<int> _points;
    public List<int> Points
    {
        get
        {
            return _points;
        }
        set
        {
            _points = value;
            if (value.Count != 4)
            {
                throw new System.Exception("points' Count must be exactly 4");
            }

            //if (DelaunayBuilder.shared.instantDraw)
                GenerateMesh();
        }
    }

    private Color _color;
    public Color Color
    {
        get
        {
            return _color;
        }
        set
        {
            //story.Add(new KeyValuePair<Tetrahedron, Color>(this, _color));
            _color = value;
           //if (DelaunayBuilder.shared.instantDraw)
            {
                if (meshRenderer)
                {
                    meshRenderer.material.color = value;
                    meshRenderer.enabled = value.a > 0;
                }
                else
                {
                    GenerateMesh();
                }
            }
        }
    }

    public MeshFilter meshFilter;
    public MeshRenderer meshRenderer;

    public int a { get { return Points[0]; } }
    public int b { get { return Points[1]; } }
    public int c { get { return Points[2]; } }
    public int d { get { return Points[3]; } }

    public Tetrahedron(int a, int b, int c, int d) : this(a, b, c, d, Random.ColorHSV()) { }

    public Tetrahedron(int a, int b, int c, int d, Color color)
    {
        disposedValue = false;

        Id = nextId++;
        Points = new int[] { a, b, c, d }.ToList();
        Color = color;

        tetrahedrons.Add(this);

        for (int i = 0; i < 4; i++)
        {
            if (!tetrahedronsForPoint.ContainsKey(_points[i]))
            {
                tetrahedronsForPoint.Add(_points[i], new HashSet<Tetrahedron>());
            }
            tetrahedronsForPoint[_points[i]].Add(this);
        }
    }

    public void CreateMeshGameObject()
    {
        if (meshFilter)
            return;

        var go = new GameObject(this.ToString());
        go.transform.parent = parentTransform;

        meshFilter = go.AddComponent<MeshFilter>();
        meshRenderer = go.AddComponent<MeshRenderer>();

        meshRenderer.material = new Material(material);
    }

    public static IEnumerable<Tetrahedron> GetTetrahedrons(params int[] pts)
    {
        var res = tetrahedronsForPoint[pts[0]].AsEnumerable();
        foreach (var p in pts)
        {
            res = res.Intersect(tetrahedronsForPoint[p]);
        }
        return res;
    }

    public static Tetrahedron GetTetrahedron(int a, int b, int c, int d)
    {
        var test = tetrahedronsForPoint[a]
            .Intersect(tetrahedronsForPoint[b])
            .Intersect(tetrahedronsForPoint[c])
            .Intersect(tetrahedronsForPoint[d]);
        var res = GetTetrahedrons(a, b, c, d);

        if (res.Count() != test.Count())
            throw new System.Exception("wrong count of found intersected shit");

        return res.SingleOrDefault();
    }

    public Tetrahedron Adjacency(int _a, int _b, int _c)
    {
        var sharedPts = new int[] { _a, _b, _c };
        var excludePoint = Points.Except(sharedPts).First();
        var arr = tetrahedronsForPoint[sharedPts[0]]
            .Intersect(tetrahedronsForPoint[sharedPts[1]])
            .Intersect(tetrahedronsForPoint[sharedPts[2]]).ToList();

        if (arr.Count == 0)
            throw new System.Exception("Are you using a disposed Tetrahedron?");

        Debug.LogError(this + " ADJACENCY " + sharedPts.JoinString() + " excl " + excludePoint + " \n" + arr.Select(x => x.ToString()).JoinString()
                       + "\n" + tetrahedronsForPoint[sharedPts[0]].JoinString()
                       + "\n" + tetrahedronsForPoint[sharedPts[1]].JoinString()
                       + "\n" + tetrahedronsForPoint[sharedPts[2]].JoinString());

        var t = arr.SingleOrDefault(x => x != this);

        if (t == null)
            return null;

        var adjPoint = t.Points.Except(sharedPts);

        t.Points = sharedPts.Concat(adjPoint).ToList();

        return t;
    }

    #region Math

    public bool ContainsPoint(int p)
    {
        return FacetsVisibleFromPoint(p) == 0;
    }

    public int FacetsVisibleFromPoint(int p)
    {
        //visible if on other side than 4th point
        bool temp;
        var dbcVisible = IsFacetVisibleFromP(d, b, c, p, a, out temp);
        var adcVisible = IsFacetVisibleFromP(a, d, c, p, b, out temp);
        var abdVisible = IsFacetVisibleFromP(a, b, d, p, c, out temp);
        var abcVisible = IsFacetVisibleFromP(a, b, c, p, d, out temp);

        int count = 0;
        if (dbcVisible) count++;
        if (adcVisible) count++;
        if (abdVisible) count++;
        if (abcVisible) count++;

        return count;
    }

    public static bool IsFacetVisibleFromP(int _a, int _b, int _c, int p, int _d, out bool pLiesOnTheFacet)
    {
        var ar = realPoints[_a];
        var br = realPoints[_b];
        var cr = realPoints[_c];
        var dr = realPoints[_d];
        var pr = realPoints[p];

        var abc = new Plane(ar, br, cr);
        var visible = !abc.SameSide(dr, pr);

        pLiesOnTheFacet = MathHelper.AreCoplanar(ar, br, cr, pr);
        if (pLiesOnTheFacet)
        {
            return false || visible;
        }

        return visible;
    }

    public bool InSphere(int p)
    {
        Vector3 _a = realPoints[a];
        Vector3 _b = realPoints[b];
        Vector3 _c = realPoints[c];
        Vector3 _d = realPoints[d];
        Vector3 _p = realPoints[p];

        return MathHelper.InSphere(_a, _b, _c, _d, _p) > MathHelper.epsilon;
    }

    #endregion

    #region Mesh Rendering

    public void DrawGizmos()
    {
        try
        {
            Vector3 _a = realPoints[a];
            Vector3 _b = realPoints[b];
            Vector3 _c = realPoints[c];
            Vector3 _d = realPoints[d];

            Gizmos.DrawLine(_a, _b);
            Gizmos.DrawLine(_a, _c);
            Gizmos.DrawLine(_a, _d);

            Gizmos.DrawLine(_b, _c);
            Gizmos.DrawLine(_b, _d);

            Gizmos.DrawLine(_c, _d);
        }
        catch { }
    }

    int[] FacetVerticesSequenceVisibleFromOutside(Vector3[] pts, params int[] indexes)
    {
        Vector3 _a = pts[indexes[0]];
        Vector3 _b = pts[indexes[1]];
        Vector3 _c = pts[indexes[2]];
        Vector3 _d = pts[indexes[3]];

        var dOrient = Mathf.Sign((float)MathHelper.Orient(_a, _b, _c, _d));
        if (dOrient > 0)
        {
            //0 1 2 3
            //a b c d
            return new int[] { indexes[0], indexes[1], indexes[2] };
        }
        else
        {
            return new int[] { indexes[2], indexes[1], indexes[0] };
        }
    }

    public void GenerateMesh()
    {
        if (!meshFilter)
        {
            CreateMeshGameObject();
        }

        if (!meshFilter)
        {
            return;
        }

        if (Points.Contains(-1))
        {
            return;
        }

        Mesh mesh = new Mesh();
        mesh.vertices = Points.Select(x => realPoints[x].point).ToArray();

        //4x vertices - each side of tetrahedron is using it's own vertices to properly calculate normals
        mesh.vertices = mesh.vertices.Concat(mesh.vertices).Concat(mesh.vertices).Concat(mesh.vertices).ToArray();

        var abc = FacetVerticesSequenceVisibleFromOutside(mesh.vertices, 0, 1, 2, 3);
        var abd = FacetVerticesSequenceVisibleFromOutside(mesh.vertices, 4, 5, 7, 6);
        var acd = FacetVerticesSequenceVisibleFromOutside(mesh.vertices, 8, 10, 11, 9);
        var bcd = FacetVerticesSequenceVisibleFromOutside(mesh.vertices, 13, 14, 15, 12);

        mesh.triangles = abc.Concat(abd).Concat(acd).Concat(bcd).ToArray();

        //mesh.triangles = new int[] { 0,1,2, 0,1,3, 0,2,3, 1,2,3  };
        Vector2[] uvs = new Vector2[mesh.vertices.Length];

        for (int i = 0; i < uvs.Length; i++)
        {
            uvs[i] = new Vector2(mesh.vertices[i].x, mesh.vertices[i].z);
        }

        mesh.uv = uvs;

        mesh.RecalculateNormals();

        meshFilter.mesh = mesh;
        meshRenderer.material.color = Color;
        meshRenderer.enabled = Color.a > 0;// && DelaunayBuilder.shared.instantDraw;
    }
    #endregion

    #region IDisposable Support
    public bool disposedValue { get; private set; } // To detect redundant calls

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                Debug.LogError("DESTROY " + this);

                GenerateMesh();

                if (meshRenderer)
                {
                    meshRenderer.gameObject.SetActive(false);
                }

                tetrahedrons.Remove(this);
                foreach (var p in Points)
                {
                    var cou = tetrahedronsForPoint[p].Count();
                    tetrahedronsForPoint[p].Remove(this);
                    if (cou - 1 != tetrahedronsForPoint[p].Count())
                        throw new System.Exception("SHAKAL");
                }

                //_points = null;
            }

            disposedValue = true;
        }
    }


    // This code added to correctly implement the disposable pattern.
    public void Dispose()
    {
        // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        Dispose(true);
    }
    #endregion
}
