﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class InSphereTest : MonoBehaviour {

    public Tetrahedron tetrahedron;

    public List<GameObject> pts;
    public bool recalc = false;
    public bool calc = false;

    public GameObject point;

	void Start () 
    {
        

	}

	private void OnDrawGizmos()
	{
        try
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(Tetrahedron.realPoints[5], 0.01f);
        }catch{}
	}

	void Update () 
    {
        if (recalc)
        {
            recalc = false;
            Tetrahedron.tetrahedrons.ElementAt(0).Dispose();
            Tetrahedron.tetrahedrons.Clear();

            Tetrahedron.realPoints = new List<VisiblePoint>{
                new Vector3(0.0f, 4.9f, -0.4f), 
                new Vector3(0.0f, 200.0f, 0.0f), 
                new Vector3(-200.0f, -200.0f, -200.0f), 
                new Vector3(0.0f, 4.7f, -0.5f),
                new Vector3(0.1f, 5.0f, -0.4f),
                new Vector3(0.0f, 5.1f, -0.4f)
            };
            var t = new Tetrahedron(0, 1, 2, 3, Color.green);
            var ta = new Tetrahedron(0, 1, 3, 4, Color.cyan);
            var p = 5;

            DelaunayBuilder.shared.StartAlgo(5);
        }
	}
}
