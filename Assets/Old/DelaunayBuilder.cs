﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class DelaunayBuilder : MonoBehaviour
{
    public static DelaunayBuilder shared;

    public bool Finished
    {
        get
        {
            return (points.Count > 4) && (points.Count <= pointToInsert);
        }
    }
    public int insertedPoints = 0;
    public bool instantDraw = true;

    public HashSet<Tetrahedron> tetrahedrons
    {
        get
        {
            return Tetrahedron.tetrahedrons;
        }
    }

    public List<VisiblePoint> points
    {
        get
        {
            return Tetrahedron.realPoints;
        }
    }

    public Material baseMaterial;
    public bool logs = true;
    public bool delays = true;
    public bool insertingEnabled = false;
    public bool expand = false;
    public float expandSpeed = 10f;
    public float expandSize = 5;

    public void AddPoint(Vector3 point)
    {
        points.Add(new VisiblePoint(point, Color.black, 0.1f));
    }

    public void AddPoint()
    {
        AddPoint(Random.insideUnitSphere * 10);
    }

    private void Awake()
    {
        shared = this;

        Tetrahedron.material = baseMaterial;
        Tetrahedron.parentTransform = transform;

        Reset();
    }

    [Range(0.01f, 0.2f)]
    public float pointGizmoSize = 0.1f;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            tetrahedrons.Add(new Tetrahedron(Random.Range(4, points.Count - 1), Random.Range(4, points.Count - 1),
                                             Random.Range(4, points.Count - 1), Random.Range(4, points.Count - 1)));
        }

        if (Input.GetKeyDown(KeyCode.P))
        {
            AddPoint();
        }

        if ((Input.GetKey(KeyCode.N) || insertingEnabled))
        {
            if (pointToInsert < points.Count)
            {
                if (StartAlgo(pointToInsert))
                {
                    pointToInsert++;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            RandomizeColors();
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            nextStep = true;
        }

        if (Input.GetKeyDown(KeyCode.Q))
            expand = !expand;

        Expand();
    }

    public bool StartAlgo(int p)
    {
        if (insertingPoint)
            return false;

        Debug.LogWarning("inserting point " + p);

        insertingPoint = true;

        Algo(p);

        return true;
    }

    public void RandomizeColors()
    {
        foreach (var t in tetrahedrons)
        {
            if (instantDraw)
                t.GenerateMesh();

            var color = Random.ColorHSV();
            color.a = 1;

            if (t.Points.Any(x => x < 4))
                color.a = 0;
            t.Color = color;
        }
    }

    private void OnDrawGizmosSelected()
    {
        foreach (Vector3 p in points)
        {
            Gizmos.color = insertingPoint && points[pointToInsert - 1] == p ? Color.red : Color.black;

            if (insertingPoint)
            {
                if (points[pointToInsert - 1] == p)
                    Gizmos.DrawSphere(p, pointGizmoSize);
            }
            else
                Gizmos.DrawSphere(p, pointGizmoSize);
        }

        foreach (Tetrahedron t in tetrahedrons)
        {
            Gizmos.color = t.Color;

            t.DrawGizmos();
        }
    }

    VisiblePoint[] RealPoints(Tetrahedron t)
    {
        return t.Points.Select(x => points[x]).ToArray();
    }

    void Expand()
    {
        foreach (var t in tetrahedrons)
        {
            var pts = RealPoints(t);
            var center = Vector3.zero;
            foreach (var p in pts)
            {
                center += (Vector3)p / 4;
            }

            if (t.meshFilter)
            {
                var targetPos = expand ? center * expandSize : Vector3.zero;

                t.meshFilter.transform.position = 
                    Vector3.Lerp(t.meshFilter.transform.position, targetPos, Time.deltaTime * expandSpeed);
            }
        }
    }

    public void Push(ref Stack<Tetrahedron> s, params Tetrahedron[] arr)
    {
        foreach (Tetrahedron t in arr)
        {
            s.Push(t);
        }
    }

    public int pointToInsert = 4;

    public bool nextStep = false;
    public float stepDelay = 1f;

    private bool insertingPoint = false;

    public void WaitForNextStep(string msg = null)
    {
        if (delays)
            nextStep = false;

        if (msg != null)
            Debug.Log(msg);

        UIController.shared.OnMessage(msg);
    }

    public void ClearColors()
    {
        foreach (var t in tetrahedrons)
        {
            t.Color = Color.clear;
        }
    }

    public int flips14, flips26, flips23, flips32, flipsM2M, flips44;

    public void Reset(bool clearPoints = true)
    {
        foreach (var t in tetrahedrons.ToList())
            t.Dispose();

        foreach (var p in points.ToList())
            p.Color = Color.black;

        tetrahedrons.Clear();

        if (clearPoints)
        {
            foreach (var p in points.ToList())
                p.Dispose();
            
            points.Clear();

            points.Add(new Vector3(-100, -100, -100));
            points.Add(new Vector3(100, -100, -100));
            points.Add(new Vector3(0, -100, 100));
            points.Add(new Vector3(0, 100, 0));
        }

        var tttt = new Tetrahedron(0, 1, 2, 3, Color.clear);

        pointToInsert = 4;
        insertedPoints = 0;
        flips14 = 0;
        flips26 = 0;
        flips23 = 0; 
        flips32 = 0; 
        flipsM2M = 0; 
        flips44 = 0;
        insertingEnabled = false;
        expand = false;
        insertingPoint = false;
    }

    Tetrahedron[] Flip14(Tetrahedron t, int p)
    {
        flips14++;

        t.Dispose();

        return new Tetrahedron[] {
            new Tetrahedron(p, t.a, t.b, t.c, Color.green),
            new Tetrahedron(p, t.a, t.b, t.d, Color.red),
            new Tetrahedron(p, t.a, t.d, t.c, Color.blue),
            new Tetrahedron(p, t.d, t.b, t.c, Color.yellow)
        };
    }

    Tetrahedron[] Flip26(Tetrahedron t, Tetrahedron ta, int[] facetWithP, int p)
    {
        flips26++;

        t.Dispose();
        ta.Dispose();

        var a = facetWithP[0];
        var b = facetWithP[1];
        var c = facetWithP[2];
        var e = t.Points.Except(facetWithP).Single();
        var f = ta.Points.Except(facetWithP).Single();

        return new Tetrahedron[] {
            new Tetrahedron(p, a, c, e, Color.black),
            new Tetrahedron(p, b, c, e, Color.blue),
            new Tetrahedron(p, a, b, e, Color.white),

            new Tetrahedron(p, a, c, f, Color.green),
            new Tetrahedron(p, b, c, f, Color.magenta),
            new Tetrahedron(p, a, b, f, Color.yellow)
        };
    }

    Tetrahedron[] Flip23(Tetrahedron t, Tetrahedron ta)
    {
        flips23++;

        t.Dispose();
        ta.Dispose();

        if (t.a == ta.d)
            throw new System.Exception();

        return (new Tetrahedron[] {
            new Tetrahedron(t.a, t.b, t.c, ta.d, Color.green),
            new Tetrahedron(t.a, t.b, t.d, ta.d, Color.red),
            new Tetrahedron(t.a, t.c, t.d, ta.d, Color.yellow)
        });
    }

    Tetrahedron[] Flip32(Tetrahedron t, Tetrahedron ta, Tetrahedron third)
    {
        flips32++;

        t.Dispose();
        ta.Dispose();
        third.Dispose();

        var sharedEdge = t.Points
                          .Intersect(ta.Points)
                          .Intersect(third.Points).ToList();

        var unsharedPoint = t.Points.FindAll(x => x != t.a).Except(sharedEdge).First();

        return (new Tetrahedron[] {
            new Tetrahedron(t.a, sharedEdge[0], unsharedPoint, ta.d, Color.green),
            new Tetrahedron(t.a, sharedEdge[1], unsharedPoint, ta.d, Color.magenta)
        });
    }

    Tetrahedron[] FlipM2M(Tetrahedron[] tets, int p, int[] edge)
    {
        flipsM2M++;
        
        foreach (var tet in tets)
        {
            tet.Dispose();
        }

        var res = new List<Tetrahedron>();

        foreach (var tet in tets)
        {
            var gf = tet.Points.Except(edge).ToArray();
            res.Add(new Tetrahedron(p, gf[0], gf[1], edge[0], Random.ColorHSV()));
            res.Add(new Tetrahedron(p, gf[0], gf[1], edge[1], Random.ColorHSV()));
        }

        return res.ToArray();
    }

    Tetrahedron[] Flip44(Tetrahedron[] tets, Tetrahedron t, Tetrahedron ta, int __a, int __b)
    {
        flips44++;

        var allpts = tets[0].Points.AsEnumerable();
        foreach (var tet in tets)
        {
            allpts = allpts.Union(tet.Points);
        }

        Debug.Log(tets.Select(x => x.ToString()).JoinString());
        Debug.Log(t + " " + ta);
        Debug.Log( "FLIP44 F: " + allpts.Except(t.Points).Except(ta.Points).JoinString());
        var __f = allpts.Except(t.Points).Except(ta.Points).Single();
        var __c = t.Points.Intersect(ta.Points).Except(new int[] { __a, __b }).Single();
        var __p = t.a;
        var __d = ta.d;
        //padf
        //pbdf
        //pcda
        //pcdb

        foreach (var del in tets.ToList())
        {
            del.Dispose();
        }

        return new Tetrahedron[] {
            new Tetrahedron(__p,__a,__d,__f, Color.cyan),
            new Tetrahedron(__p,__b,__d,__f, Color.black),
            new Tetrahedron(__p,__c,__d,__a, Color.yellow),
            new Tetrahedron(__p,__c,__d,__b, Color.white)
        };
    }

    Tetrahedron[] GetConfig44(Tetrahedron t, Tetrahedron ta, out int a, out int b)
    {
        Vector3 pr = points[t.a];
        Vector3 ar = points[ta.a];
        Vector3 br = points[ta.b];
        Vector3 cr = points[ta.c];
        Vector3 dr = points[ta.d];

        //find what edge pd intersects
        //if pd intersects ab
        Tetrahedron[] config44 = null;

        a = -1;
        b = -1;

        if (MathHelper.AreCoplanar(pr, ar, br, dr))
        {
            config44 = Tetrahedron.tetrahedronsForPoint[ta.a]
                                  .Intersect(Tetrahedron.tetrahedronsForPoint[ta.b]).ToArray();
            a = ta.a;
            b = ta.b;
        }
        else if (MathHelper.AreCoplanar(pr, br, cr, dr))
        {
            config44 = Tetrahedron.tetrahedronsForPoint[ta.b]
                                  .Intersect(Tetrahedron.tetrahedronsForPoint[ta.c]).ToArray();
            a = ta.b;
            b = ta.c;
        }
        else if (MathHelper.AreCoplanar(pr, ar, cr, dr))
        {
            config44 = Tetrahedron.tetrahedronsForPoint[ta.a]
                                  .Intersect(Tetrahedron.tetrahedronsForPoint[ta.c]).ToArray();
            a = ta.a;
            b = ta.c;
        }

        return config44;
    }

    void Algo(int p)
    {
        Stack<Tetrahedron> s = new Stack<Tetrahedron>();

        if (logs)
            ClearColors();


        points[p].Color = Color.red;
        points[p].size = 1;

        if (logs)
            WaitForNextStep("Добавление точки " + (p - 3));

        WalkToTetrahedronContainingPoint(p);
        var t = walkingResult;
        Debug.Log("walkingResult " + t);

        if (t == null)
        {
            //Debug.Log("p is too close to another point, ignoring that point");

            if (logs)
                WaitForNextStep("Точка будет пропущена, так как находится очень близко к другой, добавленной ранее");
            if (logs)
                RandomizeColors();
            
            insertingPoint = false;
            points[p].Color = Color.clear;
            return;
        }

        // check if any edge containing the point
        var checks = new int[][]
            {
                new int[] { t.d, t.b, t.c },
                new int[] { t.a, t.d, t.c },
                new int[] { t.a, t.b, t.d },
                new int[] { t.a, t.b, t.c }
            };

        List<int[]> containingFacet = new List<int[]>();

        Debug.Log(t);

        foreach (var c in checks)
        {
            if (MathHelper.AreCoplanar(points[c[0]], points[c[1]], points[c[2]], points[p]))
            {
                containingFacet.Add(c);
            }
        }

        Debug.Log("containingFacet.Count " + containingFacet.Count);

        if (containingFacet.Count == 0)
        {
            // p is connected into T by the flip 14  (if p lies inside T )

            t.Color = Color.red;

            if (logs)
                WaitForNextStep("Flip14 будет выполнен, точка внутри тетраэдра");

            t.Dispose();

            Push(ref s, Flip14(t, p));

            if (logs)
                WaitForNextStep("Создано 4 новых тетраэдра");
        }
        else if (containingFacet.Count == 2)
        {
            var edge = containingFacet[0].Intersect(containingFacet[1]).ToArray();
        
            // or by the flip m2m
            // (if p lies in an edge of T)

            //edge e1 e2

            var tets = Tetrahedron.GetTetrahedrons(edge[0], edge[1]).ToArray();

            foreach (var tet in tets)
            {
                tet.Color = Random.ColorHSV();
            }

            if (logs)
                WaitForNextStep("FlipM2M будет выполнен, точка находится на ребре тетраэдра");
            
            Push(ref s, FlipM2M(tets, p, edge));

            if (logs)
                WaitForNextStep("FlipM2M выполнен");
        }
        else if (containingFacet.Count == 1)
        {
            // by the flip 26
            // (if p lies in a face of T),

            t.Color = Color.red;
            var ta = t.Adjacency(containingFacet[0][0], containingFacet[0][1], containingFacet[0][2]);
            ta.Color = Color.yellow;

            if (logs)
                WaitForNextStep("FLIP26 будет выполнен, точка находится на грани тетраэдра");
            
            Push(ref s, Flip26(t, ta, containingFacet[0], p));

            if (logs)
                WaitForNextStep("Flip26 выполнен");
        }



        while (s.Count > 0)
        {
            if (logs)
                ClearColors();

            t = s.Pop();
            if (t.disposedValue)
                continue;

            var ta = t.Adjacency(t.b, t.c, t.d);
            if (ta == null)
            {
                
                t.Color = Color.black;
                if (logs)
                    WaitForNextStep("Для этого тетраэдра нет смежного по грани, противолежащей вставленной точке");

                continue;
            }

            t.Color = Color.green;
            ta.Color = Color.yellow;
            if (logs)
                WaitForNextStep("Найден смежный тетраэдр (желтый) по грани, противолежащей вставленной точке");

            if (t.InSphere(ta.d))
            {
                // t and ta are not Delaunay and must be deleted
                t.bad = true;
                ta.bad = true;

                //Debug.Log("count : " + points.Count + "  insert: " + pointToInsert);
                int visibleFacets = ta.FacetsVisibleFromPoint(t.a);
                //Debug.Log("Visible Facets: "+visibleFacets);

                if (logs)
                    WaitForNextStep("Четвертая точка желтого тетраэдра попадает в сферу, описывающую зеленый тетраэдр. "+
                                                 "Невыплукых общих ребер: " + (visibleFacets-1));
                
                //CheckCoplanar(t, ta);
                /* 
                 * The face f is flippable (i.e.can be transformed by a flip) 
                 * if all edges of f are convex or if each reflex edge ei 
                 * is incident to exactly three tetrahedra
                 *
                 * Note that if a face is flippable, then the number 
                 * of reflex edges determine the type of flip to be used: 
                 *      a face without reflex edges is flippable with the flip 23
                 *      a face with one reflex edge is flippable with the flip 32
                 *      a face with two reflex edges is flippable with the flip 41
                 */

                // all convex
                if (visibleFacets == 1)
                {
                    //flip23
                    //Debug.Log("Flip23");

                    int a, b;
                    var config44 = GetConfig44(t, ta, out a, out b);


                    if (config44 != null)
                    {
                        if (config44.Count() == 4)
                        {

                            t.Color = Color.green;
                            ta.Color = Color.yellow;
                            var adj = config44.Except(new Tetrahedron[] { t, ta }).ToList();
                            adj[0].Color = Color.blue;
                            adj[1].Color = Color.white;

                            if (logs)
                                WaitForNextStep("Flip44 будет выполнен");
                            
                            Push(ref s, Flip44(config44, t, ta, a, b));

                            if (logs)
                                WaitForNextStep("Flip44 выполнен");
                        }
                        else
                        {
                            //if (logs)
                            //    WaitForNextStep("config44 not found, nothing to flip");
                        }
                    }
                    else
                    {
                        t.Color = Color.green;
                        ta.Color = Color.yellow;

                        if (logs)
                            WaitForNextStep("Flip23 будет выполнен");

                        Push(ref s, Flip23(t, ta));

                        if (logs)
                            WaitForNextStep("Flip23 выполнен");
                    }
                }
                // one reflex
                else if (visibleFacets == 2)
                {
                    var thirdt = Tetrahedron.GetTetrahedron(t.a, ta.a, ta.b, ta.d);

                    if (thirdt == null)
                        thirdt = Tetrahedron.GetTetrahedron(t.a, ta.a, ta.c, ta.d);

                    if (thirdt == null)
                        thirdt = Tetrahedron.GetTetrahedron(t.a, ta.b, ta.c, ta.d);

                    // is incident to exactly three tetrahedra
                    if (thirdt != null)
                        thirdt.Color = Color.blue;

                    if (logs)
                        WaitForNextStep((thirdt != null) ? "Flip32 будет выполнен" : "Flip32 не может быть выполнен, нет третьего необходимого тетраэдра");

                    if (thirdt != null)// FacetIsVisibleFromP(ta.a, ta.b, ta.d, p, ta.c))
                    {
                        //if (MathHelper.AreCoplanar(pr, ar, cr, dr))
                        {
                            
                        }


                        Push(ref s, Flip32(t, ta, thirdt));

                        if (logs)
                            WaitForNextStep("Flip32 выполнен");
                    }
                }
                else
                {
                    //do nothing
                }
            }
        }

        if (logs)
            RandomizeColors();

        insertedPoints++;

        points[p].Color = Color.green;
        points[p].size = 0.2f;

        if (logs)
            WaitForNextStep("Точка добавлена");

        insertingPoint = false;
    }

    Tetrahedron walkingResult;
    //int[] containingFacet;

    public void WalkToTetrahedronContainingPoint(int p)
    {
        walkingResult = null;
        //containingFacet = null;

        var t = tetrahedrons.ElementAt(Random.Range(0, tetrahedrons.Count));
        var told = t;

        HashSet<Tetrahedron> visited = new HashSet<Tetrahedron>();

        while (true)
        {
            if (visited.Contains(t))
            {
                t = tetrahedrons.ElementAt(Random.Range(0, tetrahedrons.Count));
                visited.Clear();
            }

            visited.Add(t);

            if (t != told)
            {
                //if (logs)
                //    ClearColors();
                told.Color = Color.clear;
                t.Color = Color.yellow;

                told = t;
            }

            WaitForNextStep("Поиск тетраэдра, содержащего новую точку");

            //Debug.Log(t.a + " " + t.b + " " + t.c + " " + t.d + " " + p);
            //Debug.Log(points.Count);

            var checks = new List<int[]> 
            { 
                new int[] { t.d, t.b, t.c, p, t.a },
                new int[] { t.a, t.d, t.c, p, t.b },
                new int[] { t.a, t.b, t.d, p, t.c },
                new int[] { t.a, t.b, t.c, p, t.d }
            };

            bool foundAdjacency = false;
            //int[] pFacet = null;
            while (checks.Count > 0)
            {
                var arr = checks[Random.Range(0, checks.Count)];
                checks.Remove(arr);

                bool temp;
                if (Tetrahedron.IsFacetVisibleFromP(arr[0], arr[1], arr[2], arr[3], arr[4], out temp))
                {
                    t = t.Adjacency(arr[0], arr[1], arr[2]);

                    if (t == null)
                    {
                        //throw new System.Exception("WALK EXCEPTION");
                        Debug.LogError("WALK EXCEPTION");
                        t = tetrahedrons.First(x => x.ContainsPoint(p));
                        break;
                    }

                    foundAdjacency = true;
                    break;
                }
            }

            if (foundAdjacency)
                continue;

            walkingResult = t;

            Debug.Log("distances " + walkingResult.Points.Select(x => (double)Vector3.Distance(points[x], points[p])).JoinString());
            var minDist = walkingResult.Points.Select(x => Vector3.Distance(points[x], points[p])).Min();
            if (minDist < 0.1)
            {
                Debug.Log(p);
                Debug.Log(walkingResult.Points.JoinString());
                Debug.Log("ignoring point with dist: " + walkingResult.Points.Select(x => (double)Vector3.Distance(points[x], points[p])).JoinString());
                walkingResult = null;
            }

            told.Color = Color.clear;
            t.Color = Color.cyan;
            WaitForNextStep("Найден тетраэдр, содержащий добавляемую точку");

            break;
        }
    }
}
