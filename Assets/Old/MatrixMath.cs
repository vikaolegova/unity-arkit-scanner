﻿using DotNetMatrix;

public class MatrixMath
{
    public static string ToString(double[,] matrix)
    {
        var s = "{";
        var i = 0;
        foreach (var item in matrix)
            s += (i++ > 0 ? i % matrix.GetLength(1) == 1 ? "},{" : "," : "") + item;
        s += '}';
        return s;
    }

    public static double Determinant(double[,] matrix)
    {
        double[][] m = new double[matrix.GetLength(0)][];

        for (int i = 0; i < matrix.GetLength(0); i++)
        {
            m[i] = new double[matrix.GetLength(1)];

            for (int j = 0; j < matrix.GetLength(1); j++)
            {
                m[i][j] = matrix[i, j];
            }
        }

        var det = GeneralMatrix.Create(m).Determinant();

        return det;
    }
}