﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsGenerator : MonoBehaviour {

    public int count = 500;
    [Range(0.01f, 0.5f)]
    public float pointGizmoSize = 0.1f;

	// Use this for initialization
	void Start () {
	}

	private void OnDrawGizmosSelected()
	{
        if (enabled)
        foreach (Vector3 p in DelaunayBuilder.shared.points)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(p, pointGizmoSize);
        }
	}

	// Update is called once per frame
	void Update () {

        if (DelaunayBuilder.shared.points.Count < count)
        {
            var c = DelaunayBuilder.shared.points.Count;
            //DelaunayBuilder.shared.AddPoint(new Vector3(Random.Range(-20, 20), Random.Range(-20, 20), Random.Range(-20, 20)));
            DelaunayBuilder.shared.AddPoint(new Vector3(c % 6, c / 6 - c / 36 * 6, c / 36));
        }
	}
}
