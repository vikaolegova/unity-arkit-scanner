﻿using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;

public class EditorObjExporter : ScriptableObject
{
    [MenuItem("Custom/Export/Export whole selection to single OBJ")]
    public static void ExportWholeSelectionToSingle()
    {

        Transform[] selection = Selection.GetTransforms(SelectionMode.Editable | SelectionMode.ExcludePrefab);

        if (selection.Length == 0)
        {
            EditorUtility.DisplayDialog("No source object selected!", "Please select one or more target objects", "");
            return;
        }

        var filename = "export";
        var done = ObjExporter.ExportWholeSelectionToSingle(selection, filename);

        if (done != null)
            EditorUtility.DisplayDialog("Objects exported", "Exported " + selection.Length + " objects to " + filename, "");
        else
            EditorUtility.DisplayDialog("Objects not exported", "Make sure at least some of your selected objects have mesh filters!", "");
    }

}

