﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenerateImageAnchorSelector : MonoBehaviour {

    private GenerateImageAnchor[] scripts;
    private int selected = 0;

	private void Awake()
	{
        scripts = GetComponents<GenerateImageAnchor>();
	}

    public void ChooseNext(Button button)
    {
        scripts[selected].Unsubscribe();

        if (++selected >= scripts.Length)
        {
            selected = 0;
        }

        scripts[selected].Subscribe();

        button.GetComponentInChildren<Text>().text = scripts[selected].referenceImage.imageName;
    }
}
