﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;
using UnityEngine.UI;
using System.Linq;
using System;

namespace UnityEngine.UI
{
    public static class XXX
    {
        public static void SetText(this Button btn, string text)
        {
            btn.GetComponentInChildren<Text>().text = text;
        }
    }
}

public class ScanUIController : MonoBehaviour
{
    public Text pointsCount;
    public Text topText;

    public Button clearBtn;
    public Button lockCubeBtn;
    public Button addPointsBtn;
    public Button triangulateBtn;
    public Button exportBtn;
    public Button backBtn;

    private UnityARVideo arVideo;
    private ObjectScanManager scanManager;
    private DelaunayBuilderGPU gpuBuilder;
    private GPUBuilderVisualizer visualizer;

    private GameObject lastHull;
    private GameObject pickBoundingBox;

    private bool arKitIsReadyToScan;

    private double volume;

    private enum State
    {
        initializing,
        settingCubePosition,
        scanning,
        triangulating,
        showing
    }

    private State m_state;
    State state {
        get {
            return m_state;
        }
        set {
            m_state = value;

            if (arKitIsReadyToScan && value == State.initializing)
            {
                m_state = State.settingCubePosition;
            }

            ShowButtons();
        }
    }

    private void Start()
    {
        arVideo = FindObjectOfType<UnityARVideo>();
        scanManager = FindObjectOfType<ObjectScanManager>();
        gpuBuilder = FindObjectOfType<DelaunayBuilderGPU>();
        visualizer = FindObjectOfType<GPUBuilderVisualizer>();
        pickBoundingBox = FindObjectOfType<PickBoundingBox>().gameObject;

        gpuBuilder.DidFinishTriangulationEvent += GpuBuilder_DidFinishTriangulationEvent;

        state = State.initializing;
    }

    private void ShowButtons()
    {
        // tetrahedras
        gpuBuilder.transform.GetChild(0).gameObject.SetActive(state == State.showing);
        // cube
        scanManager.LockCube(state != State.settingCubePosition);
        scanManager.ShowCube(state == State.settingCubePosition || state == State.scanning);

        // UI:

        // buttons
        lockCubeBtn.gameObject.SetActive(state == State.settingCubePosition);
        addPointsBtn.gameObject.SetActive(state == State.scanning);
        clearBtn.gameObject.SetActive(state == State.scanning);
        triangulateBtn.gameObject.SetActive(state == State.scanning);
        exportBtn.gameObject.SetActive(state == State.showing);
        backBtn.gameObject.SetActive(state == State.showing);

        // point count
        pointsCount.transform.parent.gameObject.SetActive(state == State.scanning);

        // top text
        switch (state)
        {
            case State.initializing:
                topText.text = "Перемещайте iPhone, чтобы определить плоскость вокруг сканируемого объекта";
                break;
            case State.settingCubePosition:
                topText.text = "Расположите кубик так, чтобы сканируемый объект был внутри него. " +
                    "Расположите куб тапом, а затем перемещайте, водя пальцем по плоскости.";
                break;
            case State.scanning:
                topText.text = "Добавьте точки";
                break;
            case State.triangulating:
                topText.text = "Триангуляция запущена...";
                break;
            case State.showing:
                topText.text = String.Format("Расположите полученный объект тапом на плоскости. Объем: {0:0.} см^3", volume * 900000);
                break;
        }
    }

	private void Update()
	{
        ShowPointsCount();

        // restrict triangulate
        triangulateBtn.interactable = scanManager.scannedPoints.Count > 0;
	}

	public void AddPoints()
    {
        scanManager.AddPoints();
    }

    public void ClearPoints()
    {
        scanManager.scannedPoints.Clear();
    }

    public void Triangulate()
    {
        gpuBuilder.SetPoints(scanManager.scannedPoints);
        gpuBuilder.RunFull();

        state = State.triangulating;
    }

    public void ShowPointsCount()
    {
        var count = scanManager.scannedPoints.Count;
        pointsCount.text = count + " точек отсканировано";
    }

    public void Export()
    {
        var path = ObjExporter.ExportWholeSelectionToSingle(new Transform[] { lastHull.transform }, "model");
        NativeShare share = new NativeShare();
        share.AddFile(path);
        share.Share();
    }

    public void Back()
    {
        ClearPoints();
        state = State.settingCubePosition;

        Destroy(lastHull);
        lastHull = null;
    }

    public void LockCube()
    {
        ClearPoints();
        state = State.scanning;
    }

    void GpuBuilder_DidFinishTriangulationEvent(double vol)
    {
        volume = vol;
        state = State.showing;

        lastHull = visualizer.GenerateHull();
    }

    public void ARKitIsReadyToScan()
    {
        arKitIsReadyToScan = true;
        if (state == State.initializing)
        {
            state = State.settingCubePosition;
        }
    }


}
