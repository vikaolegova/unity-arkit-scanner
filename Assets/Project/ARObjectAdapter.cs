﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;
using System.IO;

[System.Serializable]
public class ARObjectAdapterData {

    public Vector3 center;
    public Vector3 extent;
    public string name;
    public Vector3[] Points;
    public UInt64[] Identifiers;

    public ARObjectAdapterData() { }

    public ARObjectAdapterData(ARReferenceObject obj)
    {
        center = obj.center;
        extent = obj.extent;
        name = obj.name;
        Points = obj.pointCloud.Points;
        Identifiers = obj.pointCloud.Identifiers;
    }

    private static string FilePath(string file)
    {
        return Path.Combine(Application.persistentDataPath, file);
    }

    public void SaveAtFullPath(string fullPath)
    {
        string jsonString = JsonUtility.ToJson(this);

        using (StreamWriter streamWriter = File.CreateText(fullPath))
        {
            streamWriter.Write(jsonString);
        }
    }

    public void Save(string file)
    {
        var path = FilePath(file);

        SaveAtFullPath(path);
    }

    public static ARObjectAdapterData Load(string path)
    {
        //var path = FilePath(file);

        using (StreamReader streamReader = File.OpenText(path))
        {
            string jsonString = streamReader.ReadToEnd();
            return JsonUtility.FromJson<ARObjectAdapterData>(jsonString);
        }
    }
}

public class ARObjectAdapter : MonoBehaviour {

    private ARObjectAdapterData data;
    public GameObject pointPref;
    public float mult = 100;
    public float psize = 0.01f;

    public bool doOffset = false;
    public float partOfPointsToShow = 0.3f;
    private Vector3 offset;

    private List<Transform> showed = new List<Transform>();

    public static Vector3[] pointsFromFile(string file)
    {
        var obj = ARObjectAdapterData.Load(file);
        return obj.Points;
    }

    void LoadDataFromFile(string file)
    {
        data = ARObjectAdapterData.Load(file);
        var goParent = new GameObject(file);
        goParent.transform.position = offset;
        if (doOffset)
        offset += Vector3.forward * 10;

        foreach (var p in data.Points)
        {
            var chance = UnityEngine.Random.Range(0f, 1f);
            if (chance > partOfPointsToShow) {
                continue;
            }

            var go = Instantiate(pointPref);
            go.transform.parent = goParent.transform;
            go.transform.position = p * mult;
            go.transform.rotation = Quaternion.identity;
        }

        showed.Add(goParent.transform);

        Debug.LogFormat("Loaded {0} points from {1}", data.Points.Length, file);
    }

	// Use this for initialization
    void Start () {
        //LoadDataFromFile("/Users/vika/Downloads/vape_1.json");
        //LoadDataFromFile("/Users/vika/Downloads/vape_5.json");
        //LoadDataFromFile("/Users/vika/Downloads/vape_9.json");
        var allPoints = new List<Vector3>(10000);
        for (int i = 0; i < 9; i++)
        {
            var fname = String.Format("/Users/vika/Downloads/objScan_{0}.json", i);
            LoadDataFromFile(fname);
            allPoints.AddRange(pointsFromFile(fname));
        }
        var fullObj = new ARObjectAdapterData();
        fullObj.Points = allPoints.ToArray();
        fullObj.SaveAtFullPath("/Users/vika/Downloads/allPoints_objScan.json");
	}
	
	// Update is called once per frame
	void Update () {
        foreach (var tt in showed)
        {
            for (int i = 0; i < tt.childCount; i++)
            {
                var t = tt.GetChild(i);
                t.transform.localScale = Vector3.one * psize;
            }
        }
	}
}
