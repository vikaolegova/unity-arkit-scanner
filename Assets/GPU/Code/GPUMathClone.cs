﻿using System.Linq;
using UnityEngine;
using float3 = UnityEngine.Vector3;

public static class GPUMathClone
{
    public struct float4x4
    {
        public readonly float[] values;
        public float4x4(params float[] values)
        {
            this.values = values;
        }
    }

    public static float inSphere(float3[] points, uint ai, uint bi, uint ci, uint di, uint pi)
    {
        float3 a = points[ai];
        float3 b = points[bi];
        float3 c = points[ci];
        float3 d = points[di];
        float3 p = points[pi];

        float4x4 m0 = new float4x4(
                 b.y, b.z, dot(b, b), 1,
                 c.y, c.z, dot(c, c), 1,
                 d.y, d.z, dot(d, d), 1,
                 p.y, p.z, dot(p, p), 1
        );
        float4x4 m1 = new float4x4(
            b.x, b.z, dot(b, b), 1,
            c.x, c.z, dot(c, c), 1,
            d.x, d.z, dot(d, d), 1,
            p.x, p.z, dot(p, p), 1
        );
        float4x4 m2 = new float4x4(
            b.x, b.y, dot(b, b), 1,
            c.x, c.y, dot(c, c), 1,
            d.x, d.y, dot(d, d), 1,
            p.x, p.y, dot(p, p), 1
        );
        float4x4 m3 = new float4x4(
            b.x, b.y, b.z, 1,
            c.x, c.y, c.z, 1,
            d.x, d.y, d.z, 1,
            p.x, p.y, p.z, 1
        );
        float4x4 m4 = new float4x4(
            b.x, b.y, b.z, dot(b, b),
            c.x, c.y, c.z, dot(c, c),
            d.x, d.y, d.z, dot(d, d),
            p.x, p.y, p.z, dot(p, p)
        );

        float detOf5x5 = a.x * determinant(m0)
                        - a.y * determinant(m1)
                        + a.z * determinant(m2)
                        - dot(a, a) * determinant(m3)
                        + 1 * determinant(m4);

        return detOf5x5 * orient(a, b, c, d); // > 0.001
    }

    public static float dot(float3 a, float3 b)
    {
        return float3.Dot(a, b);
    }

    public static float determinant(float4x4 mat)
    {
        double[,] matrix = new double[4, 4];
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                matrix[i, j] = mat.values[i * 4 + j];
            }
        }

        return (float)MatrixMath.Determinant(matrix);
    }

    public static float orient(float3 a, float3 b, float3 c, float3 p)
    {
        return determinant(new float4x4(
            a.x, a.y, a.z, 1,
            b.x, b.y, b.z, 1,
            c.x, c.y, c.z, 1,
            p.x, p.y, p.z, 1));
    }

    private static int xorForfloatIntConversion(int intVal)
    {
        return (intVal >= 0) ? intVal : intVal ^ 0x7FFFFFFF;
    }

    public static int floatToOrderedInt(float floatVal)
    {
        byte[] raw = System.BitConverter.GetBytes(floatVal);
        int intVal = System.BitConverter.ToInt32(raw, 0);
        int ordered = xorForfloatIntConversion(intVal);
        return ordered;
    }

    public static float orderedIntToFloat(int intVal)
    {
        int unordered = xorForfloatIntConversion(intVal);
        byte[] raw = System.BitConverter.GetBytes(unordered);
        float f = System.BitConverter.ToSingle(raw, 0);
        return f;
    }

    public static int facetsVisibleFromPoint(float3[] vertices, int4 t, uint p)
    {
        // every line of this matrix is a facet with the fourth point of tetrahedra
       
        int visibleFacetsCount = 0;

        foreach (var di in t.pts)
        {
            var abc = t.pts.ToList(); abc.Remove(di);
            Debug.Assert(abc.Distinct().Count() == 3);

            float3 a = vertices[abc[0]];
            float3 b = vertices[abc[1]];
            float3 c = vertices[abc[2]];
            float3 d = vertices[di];
            float3 e = vertices[p];

            var sideD = sign(orient(a, b, c, d));
            var sideE = sign(orient(a, b, c, e));

            if (sideD == 0 || sideE == 0)
            {
                return -1;
            }

            if (sideD + sideE == 0)
            {
                ++visibleFacetsCount;
            }
        }

        return visibleFacetsCount;
    }

    public static float isFlat(float3[] vertices, uint4 t)
    {
        float3 a = vertices[t.a];
        float3 b = vertices[t.b];
        float3 c = vertices[t.c];
        float3 d = vertices[t.d];
        return (orient(a, b, c, d));
    }

    public static int sign(float f)
    {
        if (f > 0) return 1;
        if (f < 0) return -1;
        return 0;
    }
}
