﻿using UnityEngine;


[System.Serializable]
public struct uint2
{
    public uint a;
    public uint b;
}

[System.Serializable]
public struct int2
{
    public int a;
    public int b;

    public int2(int a, int b)
    {
        this.a = a;
        this.b = b;
    }
}

[System.Serializable]
public struct uint4
{
    public uint a;
    public uint b;
    public uint c;
    public uint d;

    public uint4(uint a, uint b, uint c, uint d)
    {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public uint[] pts
    {
        get
        {
            return new uint[4] { a, b, c, d };
        }
    }

    public Vector3 GetCenterWith(Vector3[] points)
    {
        return (points[a] + points[b] + points[c] + points[d]) / 4;
    }
}

[System.Serializable]
public struct int3
{
    public int a;
    public int b;
    public int c;

    public int3(int a, int b, int c)
    {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public int[] pts
    {
        get
        {
            return new int[3] { a, b, c };
        }
    }
}

[System.Serializable]
public struct int4
{
    public int a;
    public int b;
    public int c;
    public int d;

    public int4(int a, int b, int c, int d)
    {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public int this[int key]
    {
        get
        {
            return pts[key];
        }
        set
        {
            if (key == 0) a = value;
            if (key == 1) b = value;
            if (key == 2) c = value;
            if (key == 3) d = value;
        }
    }

    public int[] pts
    {
        get
        {
            return new int[4] { a, b, c, d };
        }
    }

    public Vector3 GetCenterWith(Vector3[] points)
    {
        return (points[a] + points[b] + points[c] + points[d]) / 4;
    }
}