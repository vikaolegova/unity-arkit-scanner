﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[System.Serializable]
public class BufferWrapper<T> : IDisposable
{
    [NonSerialized]
    public ComputeBuffer buf;
    public T[] data;
    public int counterValue;

    private string shaderName;
    private ComputeShader shader;

    public BufferWrapper() { }

    public BufferWrapper(ComputeShader shader,
                            string shaderBufferName,
                            int elementSize,
                            ComputeBufferType type = ComputeBufferType.Default)
    {
        buf = new ComputeBuffer(DelaunayBuilderGPU.BUFSIZE, elementSize, type);
        counterValue = 0;
        this.shaderName = shaderBufferName;
        this.shader = shader;
    }

    public void Dispose()
    { 
        buf.Release();
    }

    public BufferWrapper<T> Alias(int kernelIndex)
    {
        shader.SetBuffer(kernelIndex, shaderName, buf);
        return this;
    }

    public T[] ReadWithSize(int size)
    {
        if (data == null)
            data = new T[size];
        if (data.Length != size)
            Array.Resize(ref data, size);
        return Read();
    }

    public T[] Read()
    {
        buf.GetData(data);
        return data;
    }

    public T[] Set(IEnumerable<T> arr)
    {
        data = arr.ToArray();
        WriteToGPU();
        return data;
    }

    public BufferWrapper<T> SetCounterValue(int counter)
    {
        buf.SetCounterValue((uint)counter);
        return this;
    }

    public BufferWrapper<T> SetCounterValue(uint counter)
    {
        buf.SetCounterValue(counter);
        return this;
    }

    public BufferWrapper<T> WriteToGPU()
    {
        buf.SetData(data);
        return this;
    }
}
