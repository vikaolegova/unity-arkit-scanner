﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GPUTest : MonoBehaviour
{
    private DelaunayBuilderGPU main;
    private ComputeShader shader;
    private KernelIds kernel;

    void Awake()
    {
        main = GetComponent<DelaunayBuilderGPU>();
        shader = main.shader;
        kernel = main.kernel;

        res = new BufferWrapper<float>(main.shader,
            "debug_inSphereResult",
            sizeof(float)).Alias(kernel.testInSphere);
        res.Set(new float[] { -322 });
    }

    void Update()
    {
        GPUTestInSphereObjects();
    }

    public GameObject sphereDebug;
    public GameObject pointDebug;
    public float inSphereDebugValue;
    private void GPUTestInSphereObjects()
    {
        Vector3 center = sphereDebug.transform.position;
        float radius = sphereDebug.transform.localScale.x / 2;

        Vector3 point = pointDebug.transform.position;

        var points = new Vector3[] {
            center + Vector3.forward * radius,
            center + Vector3.right * radius,
            center + Vector3.up * radius,
            center - Vector3.right * radius,
            point
            };

        main.bufVertices.Alias(kernel.testInSphere).Set(points);

        inSphereDebugValue = GPUTestInSphere();
    }

    BufferWrapper<float> res;
    public float GPUTestInSphere()
    {
        res.Alias(kernel.testInSphere);
        shader.Dispatch(kernel.testInSphere, 1, 1, 1);

        res.Read();

        return res.data[0];
    }
}
