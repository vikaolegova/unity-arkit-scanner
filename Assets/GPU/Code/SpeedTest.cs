﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class SpeedTest : MonoBehaviour
{
    [Range(0.01f, 0.5f)]
    public float pointGizmoSize = 0.1f;

    private Vector3[] points;
    private DelaunayBuilderGPU gpuBuilder;
    private Stopwatch sw = new Stopwatch();

    private void Awake()
    {
        FindObjectOfType<ScanUIController>().gameObject.SetActive(false);
    }

    void Start()
    {
        gpuBuilder = FindObjectOfType<DelaunayBuilderGPU>();
        gpuBuilder.DidFinishTriangulationEvent += GpuBuilder_DidFinishTriangulationEvent;
        gpuBuilder.calcVolume = false;

        Measure(1000);
    }

    private void Update()
    {
        if (!sw.IsRunning)
        {
            Measure(points.Length + 1000);
        }
    }

    void Measure(int pointCount)
    {
        if (pointCount > 30000) return;

        points = GeneratePointsCube(pointCount);
        gpuBuilder.SetPoints(points);

        sw.Start();

        gpuBuilder.RunFull();
    }

    void GpuBuilder_DidFinishTriangulationEvent(double volume)
    {
        sw.Stop();
        UnityEngine.Debug.LogWarningFormat("{0}: \t{1}", points.Length, sw.Elapsed);

        sw.Reset();
    }

    Vector3[] GeneratePointsCube(int count)
    {
        Vector3[] res = new Vector3[count];

        var size = Mathf.RoundToInt(Mathf.Pow(count, 1f / 3));
        print(size);
        for (int i = 0; i < count; i++)
        {
            //res[i] = new Vector3(i % size, i / size - i / (size * size) * size, i / (size * size));
            res[i] = UnityEngine.Random.insideUnitSphere;
        }
        return res;
    }
}
