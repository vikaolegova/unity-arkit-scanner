﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public struct KernelIds
{
    public int main;
    public int locations;
    public int FindInserts;
    public int CheckInserts;
    public int insertPoint;
    public int findFlips;
    public int doFlips;
    public int UpdateAdjacents;

    public int testInSphere;

    public string NameOfKernel(int kernel)
    {
        if (kernel == main) return "CSMain";
        if (kernel == locations) return "FindLocations";
        if (kernel == FindInserts) return "FindInserts";
        if (kernel == CheckInserts) return "CheckInserts";
        if (kernel == insertPoint) return "InsertPoint";
        if (kernel == findFlips) return "FindFlips";
        if (kernel == doFlips) return "DoFlips23";
        if (kernel == UpdateAdjacents) return "UpdateAdjacents";
        if (kernel == testInSphere) return "DebugInSphere";
        return "UNNAMED";
    }

    public KernelIds(ComputeShader computeShader)
    {
        main = computeShader.FindKernel("CSMain");
        locations = computeShader.FindKernel("FindLocations");
        FindInserts = computeShader.FindKernel("FindInserts");
        insertPoint = computeShader.FindKernel("InsertPoint");
        findFlips = computeShader.FindKernel("FindFlips");
        doFlips = computeShader.FindKernel("DoFlips23");
        UpdateAdjacents = computeShader.FindKernel("UpdateAdjacents");
        CheckInserts = computeShader.FindKernel("CheckInserts");

        testInSphere = computeShader.FindKernel("DebugInSphere");
    }
}

public class DelaunayBuilderGPU : MonoBehaviour
{
    public bool useAsserts = false;

    public ComputeShader shader;
    public Texture texture;
    public RenderTexture renderTexture;

    public KernelIds kernel;

    public Vector3[] pointsUnscaled;

    public Vector3[] points;
    public int4[] tetrahedrons;
    public int[] location;
    public int4[] nonDelaunayVisibleFacets;
    public int[] insert;
    public int[] tetrahedrasToCheck;
    public List<int> pointsToInsert;
    public int3[] flipWithTAndPoint;
    public int4[] adjacent;
    public int4[] adjacentPrevious;
    public int4[] replaced;

    public BufferWrapper<Vector3> bufVertices;
    public BufferWrapper<int> bufLocation;
    public BufferWrapper<int> bufDistances;
    public BufferWrapper<int4> bufNonDelaunayVisibleFacets;
    public BufferWrapper<int> bufInsert;
    public BufferWrapper<int4> bufTetrahedras;
    public BufferWrapper<int> bufTetrahedrasToCheck;
    public BufferWrapper<int> bufPointsToInsert;
    public BufferWrapper<int4> bufAdjacent;
    public BufferWrapper<int4> bufAdjacentUpdated;
    public BufferWrapper<int4> bufReplaced;

    public BufferWrapper<int3> bufFlipWithTAndPoint;

    public delegate void DidFinishTriangulation(double volume);
    public event DidFinishTriangulation DidFinishTriangulationEvent;

    [NonSerialized]
    public bool calcVolume = true;

    void Awake()
    {
        kernel = new KernelIds(shader);
        renderTexture = new RenderTexture(64, 64, 24);
        renderTexture.enableRandomWrite = true;
        renderTexture.Create();

        shader.SetTexture(kernel.main, "Result", renderTexture);
        Dispatch(kernel.main, 64, 64);

        var rend = GetComponent<Renderer>();
        rend.material = new Material(rend.material);
        rend.material.SetTexture("_MainTex", renderTexture);

        FirstRun();

        RandomRun();
    }

    private void OnDestroy()
    {
        bufVertices.Dispose();
        bufLocation.Dispose();
        bufDistances.Dispose();
        bufNonDelaunayVisibleFacets.Dispose();
        bufInsert.Dispose();
        bufTetrahedras.Dispose();
        bufTetrahedrasToCheck.Dispose();
        bufPointsToInsert.Dispose();
        bufAdjacent.Dispose();
        bufAdjacentUpdated.Dispose();
        bufReplaced.Dispose();
        bufFlipWithTAndPoint.Dispose();
        bufCounter.Dispose();
    }

    void FirstRun()
    {
        CreateBuffers();
        ResetBuffers();
    }

    void RandomRun()
    {
#if UNITY_EDITOR
        StartWithPoints(ARObjectAdapter.pointsFromFile("allPoints_objScan.json"));
#endif
    }

    void StartWithPoints(IEnumerable<Vector3> collection)
    {
        SetPoints(collection);

        FindInserts();
        InsertPoints();
    }

    public void RunFull()
    {
        autoInsert = true;
        enabled = true;
    }

    public void SetPoints(IEnumerable<Vector3> newPoints)
    {
        var initialSize = 100f;

        var arr = new List<Vector3>
        {
            new Vector3(-1, -1, -1) * initialSize,
            new Vector3(1, -1, -1) * initialSize,
            new Vector3(0, -1, 1) * initialSize,
            new Vector3(0, 1, 0) * initialSize
        };

        //var maxMagnitude = 50 + newPoints.Max(x => x.magnitude);

        //arr.Capacity = 4 + 100 + newPoints.Count();
        //for (int i = 0; i < 100; i++)
        //{
        //    var p = UnityEngine.Random.onUnitSphere * maxMagnitude;
        //    arr.Add(p);
        //}

        //print(maxMagnitude);

        pointsUnscaled = newPoints.ToArray();

        var pointsScalerForOutput = Mathf.Sqrt(newPoints.Max(x => x.sqrMagnitude));

        arr.AddRange(newPoints.Select((x) => x / pointsScalerForOutput));
        points = arr.ToArray();

        Debug.LogFormat("Will insert {0} points", points.Length - 4);
        pointsToInsert = Enumerable.Range(4, points.Length - 4).ToList();

        ResetBuffers();
    }

    public static int BUFSIZE = 300000;
    void ResetBuffers()
    {
        bufVertices.Set(points);

        var ptstoins = Enumerable.Repeat(-1, BUFSIZE).ToArray();
        for (int i = 0; i < pointsToInsert.Count; i++)
            ptstoins[i] = pointsToInsert[i];
        bufPointsToInsert.Set(ptstoins);

        var tets = Enumerable.Repeat(new int4(-1, -1, -1, -1), BUFSIZE).ToArray();
        tets[0] = new int4(0, 1, 2, 3);
        tetrahedrons = new int4[] { tets[0] };
        bufTetrahedras.SetCounterValue(1).Set(tets);

        location = bufLocation.Set(Enumerable.Repeat(0, BUFSIZE));
        bufDistances.Set(Enumerable.Repeat(0, BUFSIZE));
        bufNonDelaunayVisibleFacets.Set(Enumerable.Repeat(new int4(-1, -1, -1, -1), BUFSIZE));
        insert = bufInsert.Set(Enumerable.Repeat(-1, BUFSIZE));
        tetrahedrasToCheck = bufTetrahedrasToCheck.SetCounterValue(0).Set(Enumerable.Repeat(-1, BUFSIZE));
        flipWithTAndPoint = bufFlipWithTAndPoint.Set(Enumerable.Repeat(new int3(-1, -1, -1), BUFSIZE));
        adjacent = bufAdjacent.Set(Enumerable.Repeat(new int4(-1, -1, -1, -1), BUFSIZE));
        bufReplaced.Set(Enumerable.Repeat(new int4(-1, -1, -1, -1), BUFSIZE));
    }

    void CreateBuffers()
    {
        bufVertices = new BufferWrapper<Vector3>(shader, "vertices", sizeof(float) * 3)
            .Alias(kernel.locations);

        bufLocation = new BufferWrapper<int>(shader, "location", sizeof(int) * 1)
            .Alias(kernel.locations);

        bufDistances = new BufferWrapper<int>(shader, "distances", sizeof(int) * 1);

        bufNonDelaunayVisibleFacets = new BufferWrapper<int4>(shader, "nonDelaunayFacetsVisible", sizeof(int) * 4);

        bufTetrahedras = new BufferWrapper<int4>(shader, "tetrahedras", sizeof(uint) * 4, ComputeBufferType.Append)
            .Alias(kernel.locations);

        bufInsert = new BufferWrapper<int>(shader, "insert", sizeof(int) * 1);

        bufTetrahedrasToCheck = new BufferWrapper<int>(shader, "facetsToCheck", sizeof(int) * 1, ComputeBufferType.Append);

        bufPointsToInsert = new BufferWrapper<int>(shader, "pointsToInsert", sizeof(int) * 1)
            .Alias(kernel.locations);

        bufFlipWithTAndPoint = new BufferWrapper<int3>(shader, "flipWithTetrahedraAndPoint", sizeof(int) * 3);

        bufAdjacent = new BufferWrapper<int4>(shader, "adjacentsIndexes", sizeof(int) * 4);
        bufAdjacentUpdated = new BufferWrapper<int4>(shader, "adjacentsIndexesUpdated", sizeof(int) * 4);

        bufReplaced = new BufferWrapper<int4>(shader, "replaced", sizeof(int) * 4);
    }

    void Assert(bool condition, string format, params object[] objects)
    {
        if (useAsserts && !condition)
            throw new Exception(string.Format(format, objects));
    }

    ComputeBuffer bufCounter;
    uint GetAppendBufferCounter<T>(ref BufferWrapper<T> buffer)
    {
        if (bufCounter == null)
            bufCounter = new ComputeBuffer(1, sizeof(uint), ComputeBufferType.Default);

        ComputeBuffer.CopyCount(buffer.buf, bufCounter, 0);
        uint[] counter = { 999 };
        bufCounter.GetData(counter);
        return counter[0];
    }

    void MarkNewlyCreatedTetrahedrasToCheckAndReadTetrahedrasArray()
    {
        int oldCountOfTetrahedras = (int)tetrahedrons.Length;
        int newCountOfTetrahedras = (int)GetAppendBufferCounter(ref bufTetrahedras);
        int tetrahedrasCreated = newCountOfTetrahedras - oldCountOfTetrahedras;
        int newCountOfTetrahedrasToCheck = (int)GetAppendBufferCounter(ref bufTetrahedrasToCheck);

        Debug.LogFormat("Tets: {0} -> {1}, \n\tcreated: {2}",
            oldCountOfTetrahedras,
            newCountOfTetrahedras,
            tetrahedrasCreated);

        // ---- GetData десериализует массив байт (которые хранятся внутри ComputeBuffer'а) в массив int'ов (ну в данном случае именно интов
        // Обновляем массив всех тетраэдров из буфера, с которым че-то сделал compute shader.
        tetrahedrasToCheck = bufTetrahedrasToCheck.ReadWithSize(newCountOfTetrahedrasToCheck);
        tetrahedrons = bufTetrahedras.ReadWithSize(newCountOfTetrahedras);
    }

    bool AssertPointsSameSide(bool expectedSameSide, int a, int b, int c, int x, int y, string msg, params object[] objs)
    {
        var aa = points[a];
        var bb = points[b];
        var cc = points[c];

        var dor = GPUMathClone.orient(aa, bb, cc, points[x]);
        var por = GPUMathClone.orient(aa, bb, cc, points[y]);

        var s1 = GPUMathClone.sign(dor);
        var s2 = GPUMathClone.sign(por);

        var repmsg = string.Format(msg, objs);

        // Ignore checking when one orient is zero (because flat tetrahedras will be checked in Flip23 kernel)
        if (s1 == 0 || s2 == 0)
        {
            return true;
        }

        Assert((s1 == s2) == expectedSameSide, repmsg + "\nOrients: {0} and {1}", dor, por);

        return (s1 == s2) == expectedSameSide;
    }

    bool tetContainsPoint(int4 t, uint p)
    {
        foreach (var tp in t.pts)
        {
            var facet = t.pts.ToList(); facet.Remove(tp);
            {
                bool assertOk = AssertPointsSameSide(true, facet[0], facet[1], facet[2], tp, (int)p, "tetContainsPoint failed:");

                if (!assertOk) return false;
            }
        }
        return true;
    }

    int uniquePointInSecond(int4 t, int4 ta)
    {
        bool[] matched = new bool[] { false, false, false, false };
        for (int i = 0; i < 4; ++i)
        {
            for (int j = 0; j < 4; ++j)
            {
                if (t[i] == ta[j]) matched[j] = true;
            }
        }

        int sameVerticesCounter = 0;
        int unique = -2;
        for (int i = 0; i < 4; i++)
        {
            if (matched[i])
                sameVerticesCounter++;
            else
                unique = ta[i];
        }

        // if not really adjacent
        if (sameVerticesCounter != 3)
        {
            return -1;
        }

        return unique;
    }

    int numberOfPointInTetrahedra(uint p, int4 t)
    {
        for (int i = 0; i < 4; i++)
            if (t[i] == (int)p)
                return i;

        return -1; // SHOULD NOT HAPPEN
    }

    void updateIfAdjacent(int ti, int tai, ref int4 newadj)
    {
        if (ti < 0) return;
        if (tai < 0) return;

        int4 t = tetrahedrons[ti];
        int4 ta = tetrahedrons[tai];

        if (ta.a < 0) return;

        int unique = uniquePointInSecond(ta, t);
        if (unique < 0)
            return;

        int numberOfUniqueInT = numberOfPointInTetrahedra((uint)unique, t);

        if (numberOfUniqueInT == 0) newadj[0] = tai;
        else if (numberOfUniqueInT == 1) newadj[1] = tai;
        else if (numberOfUniqueInT == 2) newadj[2] = tai;
        else if (numberOfUniqueInT == 3) newadj[3] = tai;
    }

    void DEBUG()
    {
        uint ti = 6;
        int4 t = tetrahedrons[ti];

        int4 newAdjacents = new int4(-1, -1, -1, -1);

        if (t.a >= 0)
        {
            /*
             все внешние соседи это либо 
             - старые соседи исходных
             - их замены
             */

            int4 mySource = replaced[ti];

            if (mySource.a < 0) mySource.a = (int)ti;

            // бежим по исходным
            for (int i = 0; i < 4; i++)
            {
                // один из исходных
                int s = mySource[i]; if (s < 0) continue;

                // проверим s
                updateIfAdjacent((int)ti, s, ref newAdjacents);

                // бежим по его соседям
                for (int j = 0; j < 4; j++)
                {
                    // один из соседей исходного
                    int aOfS = adjacent[s][j]; if (aOfS < 0) continue;

                    // проверим aOfS
                    updateIfAdjacent((int)ti, aOfS, ref newAdjacents);

                    // бежим по заменам соседа исходного
                    for (int h = 0; h < 4; h++)
                    {
                        // одна из замен соседа исходного
                        int repOfAofS = replaced[aOfS][h]; if (repOfAofS < 0) continue;

                        // проверим repOfAofS
                        updateIfAdjacent((int)ti, repOfAofS, ref newAdjacents);
                    }
                }
            }
        }

    }

    void CheckAsserts()
    {
        if (!useAsserts)
            return;

        Debug.Assert(tetrahedrasToCheck.Distinct().Count() == tetrahedrasToCheck.Count(),
            "Dupes in tetrahedrasToCheck");

        for (int i = 0; i < tetrahedrons.Length; ++i)
        {
            var x = tetrahedrons[i];
            Assert((x.pts.Distinct().Count() == 4) || x.pts.Distinct().SingleOrDefault() == -1, // NOTE: IGNORE REMOVED TETS
                "Dupes in {0}: {1} {2} {3} {4}", i, x.a, x.b, x.c, x.d);
        }

        for (int i = 0; i < tetrahedrons.Length - 1; i++)
        {
            for (int j = i + 1; j < tetrahedrons.Length; j++)
            {
                var t = tetrahedrons[i];
                var ta = tetrahedrons[j];

                var union = t.pts.Intersect(ta.pts).ToArray();

                if (union.Count() == 3)
                {
                    var a = t.pts.Except(union).Single();
                    var b = ta.pts.Except(union).Single();

                    string s = "rep: " + replaced[i].pts.JoinString() + " " + replaced[j].pts.JoinString() + "\n"
                                                    + "adj: " + adjacent[i].pts.JoinString() + " " + adjacent[j].pts.JoinString() + "\n"
                                                    + "pts: " + tetrahedrons[i].pts.JoinString() + " " + tetrahedrons[j].pts.JoinString() + "\n";

                    Assert(adjacent[i].pts.Contains(j), "AAAAAAA {0} {1}\n{2}", i, j, s);

                    AssertPointsSameSide(false, union[0], union[1], union[2], a, b,
                        "SAME SIDE ADJ TETS {0} {1} (pts: {2} and {3})", i, j, a, b);
                }
            }
        }

        //for (int i = 0; i < tetrahedrons.Length - 1; i++)
        //{
        //    for (int j = 0; j < 4; j++)
        //    {
        //        var t = tetrahedrons[i];
        //        var ta = tetrahedrons[adjacent[i].pts[j]];

        //        var union = t.pts.Intersect(ta.pts).ToArray();

        //        if (union.Count() == 3)
        //        {
        //            var a = t.pts.Except(union).Single();
        //            var b = ta.pts.Except(union).Single();

        //            AssertPointsSameSide(false, union[0], union[1], union[2], a, b,
        //                "SAME SIDE ADJ TETS {0} {1} (pts: {2} and {3})", i, j, a, b);
        //        }
        //    }
        //}
    }

    void Dispatch(int k, int x, int y = 1)
    {
        uint groupSizeX;
        uint groupSizeY;
        uint groupSizeZ;
        shader.GetKernelThreadGroupSizes(k, out groupSizeX, out groupSizeY, out groupSizeZ);

        int numGroupsX = (int)(x / groupSizeX + (x % groupSizeX > 0 ? 1 : 0));
        int numGroupsY = (int)(y / groupSizeY + (y % groupSizeY > 0 ? 1 : 0));

        Debug.LogFormat("Dispatch " + kernel.NameOfKernel(k) + " {0}x{1}x{2} ({3}*{4}x{5}*{6}x1)",
                        x, y, 1,
                        groupSizeX, numGroupsX, groupSizeY, numGroupsY);

        shader.Dispatch(k, numGroupsX, numGroupsY, 1);
    }

    void UpdateAdjacents()
    {
        //bufAdjacent.Set(Enumerable.Repeat(new int4(-1, -1, -1, -1), tetrahedrons.Length));

        var k = kernel.UpdateAdjacents;

        bufTetrahedras.Alias(k);
        bufAdjacent.Alias(k);
        bufAdjacentUpdated.Alias(k);
        bufReplaced.Alias(k);

        Dispatch(k, tetrahedrons.Length);

        adjacentPrevious = new int4[adjacent.Length];
        adjacent.CopyTo(adjacentPrevious, 0);

        adjacent = bufAdjacentUpdated.ReadWithSize(tetrahedrons.Length);
        bufAdjacent.Set(adjacent);

        CheckAsserts();
    }

    void UpdateLocations()
    {
        var k = kernel.locations;

        bufReplaced.Alias(k);

        Dispatch(k, pointsToInsert.Count);

        location = bufLocation.ReadWithSize(points.Length);

        Debug.LogFormat("{0} points have no location", pointsToInsert.Count(x => x >= 0 && location[x] < 0));
    }

    void FindInserts()
    {
        if (pointsToInsert.Count == 0)
        {
            Debug.Log("pointsToInsert is empty");
            return;
        }

        location = bufLocation.ReadWithSize(points.Length);
        insert = Enumerable.Repeat(-2, tetrahedrons.Length).ToArray();

        // remove points without location from insertion stack
        for (int i = 0; i < pointsToInsert.Count; i++)
        {
            int p = pointsToInsert[i];
            if (p < 0) continue;
            if (location[p] < 0) pointsToInsert[i] = -1;
        }

        var k = kernel.FindInserts;
        var k2 = kernel.CheckInserts;

        bufInsert.Set(Enumerable.Repeat(-1, tetrahedrons.Length));
        bufDistances.Set(Enumerable.Repeat(-1, tetrahedrons.Length));
        //bufNonDelaunayVisibleFacets.Set(Enumerable.Repeat(new int4(-1,-1,-1,-1), tetrahedrons.Length));

        bufTetrahedras.Alias(k).Alias(k2);
        bufVertices.Alias(k).Alias(k2);
        bufLocation.Alias(k).Alias(k2);
        bufInsert.Alias(k).Alias(k2);
        bufDistances.Alias(k).Alias(k2);
        //bufNonDelaunayVisibleFacets.Alias(k).Alias(k2);
        bufPointsToInsert.Alias(k).Alias(k2);

        Dispatch(k, pointsToInsert.Count);
        Dispatch(k2, pointsToInsert.Count);

        insert = bufInsert.ReadWithSize(tetrahedrons.Length);
        //nonDelaunayVisibleFacets = bufNonDelaunayVisibleFacets.ReadWithSize(tetrahedrons.Length).ToArray();
    }

    void InsertPoints()
    {
        if (pointsToInsert.Count == 0)
        {
            Debug.Log("pointsToInsert is empty");
            return;
        }

        if (useAsserts)
        {
            var indexesOfPointsToInsert = insert.Except(new int[] { -1 });
            Assert(indexesOfPointsToInsert.Count() == indexesOfPointsToInsert.Distinct().Count(),
                "Dupes in insert array");

            for (int i = 0; i < tetrahedrons.Length; i++)
            {
                var t = tetrahedrons[i];
                var pint = insert[i];
                if (pint < 0)
                {
                    continue;
                }
                var willInsert = pint;

                Assert(t.pts.Contains(willInsert) == false, "Attemp to insert already existing point into {0}", i);
                Assert(tetContainsPoint(t, (uint)willInsert), "Doesn't contain {0} in {1}", willInsert, i);
            }
        }

        if (useAsserts)
            Debug.Assert(tetrahedrasToCheck.Length == 0);

        bufReplaced.Set(Enumerable.Repeat(new int4(-1, -1, -1, -1), tetrahedrons.Length));

        var k = kernel.insertPoint;

        bufInsert.Alias(k);
        bufTetrahedras.Alias(k);
        bufTetrahedrasToCheck.Alias(k);
        bufAdjacent.Alias(k);
        bufReplaced.Alias(k);

        Dispatch(k, tetrahedrons.Length);

        MarkNewlyCreatedTetrahedrasToCheckAndReadTetrahedrasArray();

        for (int i = 0; i < insert.Length; ++i)
        {
            var ind = pointsToInsert.IndexOf(insert[i]);
            if (ind >= 0) pointsToInsert[ind] = -1;
        }
        bufPointsToInsert.Set(pointsToInsert);

        replaced = bufReplaced.ReadWithSize(tetrahedrons.Length);

        UpdateAdjacents();
        UpdateLocations();
    }

    public int4[] tets;
    void FindFlips()
    {
        bufFlipWithTAndPoint
            .Alias(kernel.findFlips)
            .Set(Enumerable.Repeat(new int3(-1, -1, -1), tetrahedrons.Length).ToArray());

        if (tetrahedrasToCheck.Length == 0)
        {
            Debug.Log("tetrahedrasToCheck is empty");
            return;
        }

        var k = kernel.findFlips;

        bufVertices.Alias(k);
        bufTetrahedras.Alias(k);
        bufTetrahedrasToCheck.Alias(k);
        bufAdjacent.Alias(k);

        Dispatch(k, tetrahedrasToCheck.Length);

        flipWithTAndPoint = bufFlipWithTAndPoint.Read();
        tetrahedrasToCheck = bufTetrahedrasToCheck.SetCounterValue(0).Set(Enumerable.Repeat(-1, tetrahedrasToCheck.Length));

        /*
         let's fix problem :        
         FlipWith: { _:5, _:5, 5:_ }
        */

        bool[] used = new bool[tetrahedrons.Length];
        for (int i = 0; i < tetrahedrons.Length; ++i)
        {
            int3 flipWith = flipWithTAndPoint[i];
            int ta = flipWith.a;
            int third = flipWith.c;
            if (ta < 0) continue;
            if (used[ta] || used[i] || (third >= 0 && used[third]))
            {
                flipWithTAndPoint[i].a = -1;
            }
            else
            {
                used[ta] = true;
                used[i] = true;
                if (third >= 0)
                {
                    used[third] = true;

                    if (useAsserts)
                    {
                        var config = new int4[3]
                        {
                        tetrahedrons[i],
                        tetrahedrons[flipWith.a],
                        tetrahedrons[flipWith.c],
                        };

                        Debug.LogFormat("Will do a flip32 with {0}, {1}, {2}", i, flipWith.a, flipWith.c);
                        Debug.LogFormat("{0}: {1}, {2}, {3}", "k", config[0].pts.JoinString(), config[1].pts.JoinString(), config[2].pts.JoinString());

                        int visible = GPUMathClone.facetsVisibleFromPoint(points, config[0], (uint)flipWith.b);
                        Assert(visible == 2, "SHOULD BE 2 VISIBLE, NOT {0}", visible);

                        Assert(adjacent[i].pts.Contains(third) && adjacent[i].pts.Contains(third), "THIRD NOT FOUND");
                    }
                }
            }
        }

        bufFlipWithTAndPoint.Set(flipWithTAndPoint);

        if (useAsserts)
        {
            // CHECK CONSISTENCY
            for (int i = 0; i < tetrahedrons.Length - 1; i++)
            {
                var adjI = flipWithTAndPoint[i].a;
                var unique = flipWithTAndPoint[i].b;
                var third = flipWithTAndPoint[i].c;

                if (adjI < 0) continue;

                var t = tetrahedrons[i];
                var ta = tetrahedrons[adjI];

                var union = t.pts.Intersect(ta.pts).ToArray();

                Debug.LogFormat("flip {0} with {1} (u: {2}) third: {3}", i, adjI, unique, third);

                Assert(union.Length == 3, "Union count = {0} for {1} and {2}", union.Length, i, adjI);
                {
                    var a = t.pts.Except(union).Single();
                    var b = ta.pts.Except(union).Single();

                    Assert(b == unique, "Unique {0} is wrong. Proper: {1} for {2}:{3}", unique, b, i, adjI);

                    // check unflippable config
                    int facetsVisible = GPUMathClone.facetsVisibleFromPoint(points, t, (uint)b);
                    Assert((facetsVisible == 1 && third < 0) ||
                           (facetsVisible == 2 && third >= 0),
                        "VISIBLE = {0}, third: {1}", facetsVisible, third);

                    // check that adjacent tetrahedras dont intersect
                    AssertPointsSameSide(false, union[0], union[1], union[2], a, b,
                        "FindFlips: SAME SIDE ADJ TETS {0} {1} (pts: {2} and {3})", i, adjI, a, b);
                }
            }
        }
    }

    void DoFlips()
    {
        bufReplaced.Set(Enumerable.Repeat(new int4(-1, -1, -1, -1), tetrahedrons.Length));

        int k = kernel.doFlips;

        bufVertices.Alias(k);
        bufTetrahedras.Alias(k);
        bufTetrahedrasToCheck.Alias(k);
        bufFlipWithTAndPoint.Alias(k);
        bufAdjacent.Alias(k);
        bufReplaced.Alias(k);

        Dispatch(k, tetrahedrons.Length);

        MarkNewlyCreatedTetrahedrasToCheckAndReadTetrahedrasArray();

        replaced = bufReplaced.ReadWithSize(tetrahedrons.Length);

        UpdateAdjacents();
        UpdateLocations();
    }

    public double GetVolume()
    {
        double volume = 0;
        var vols = new List<double>();

        for (int ti = 0; ti < tetrahedrons.Length; ti++)
        {
            int4 t = tetrahedrons[ti];
            if (t.pts.Any(x => x < 4)) continue;

            t.a -= 4;
            t.b -= 4;
            t.c -= 4;
            t.d -= 4;
            var tetvol = VolumeOfTetrahedara(pointsUnscaled, t);
            volume += tetvol;
            vols.Add(tetvol);

        }

        Debug.Log("Volumes: " + vols.JoinString());

        Debug.Log("Vol: " + volume);

        return volume;
    }

    public Mesh ExportMeshFromExternalFacets()
    {
        Mesh m = new Mesh
        {
            vertices = pointsUnscaled
        };

        List<int[]> triangles = new List<int[]>();

        int nonObjectPoints = 4; // TODO: SET 104

        for (int ti = 0; ti < tetrahedrons.Length; ti++)
        {
            int4 t = tetrahedrons[ti];

            if (t.pts.Any(x => x < nonObjectPoints)) continue;

            for (int pindex = 0; pindex < 4; pindex++)
            {
                int tai = adjacent[ti].pts[pindex];

                if (tai < 0) continue;

                int4 ta = tetrahedrons[tai];

                if (ta.pts.Any(x => x < 0)) continue;

                if (ta.pts.Any(x => x < nonObjectPoints))
                {
                    var union = t.pts.Intersect(ta.pts).ToList();
                    var single = t.pts.Except(union).SingleOrDefault();
                    if (single == 0)
                    {
                        continue;
                    }

                    union.Add(single);

                    if (union.Count() != 4 || union.Any(x => x < 0))
                    {
                        continue;
                    }
                    var facet = FacetVerticesSequenceVisibleFromOutside(points, union.ToArray())
                        .Select(x => x - 4)
                        .ToArray();
                    triangles.Add(facet);
                }
            }
        }

        m.subMeshCount = triangles.Count();
        for (int i = 0; i < m.subMeshCount; i++)
        {
            m.SetTriangles(triangles[i], i);
        }

        //m.triangles = triangles.ToArray();

        Debug.LogFormat("Generated triangles: {0}", m.triangles.Length / 3);

        // UVs:
        Vector2[] uvs = new Vector2[m.vertices.Length];

        for (int i = 0; i + 2 < uvs.Length; i += 3)
        {
            uvs[i] = Vector2.zero;
            uvs[i + 1] = Vector2.right;
            uvs[i + 2] = Vector2.one;// new Vector2(0.5f, 1);
        }

        m.uv = uvs;

        // Normals
        m.RecalculateNormals();
        m.RecalculateBounds();

        return m;
    }

    int[] FacetVerticesSequenceVisibleFromOutside(Vector3[] pts, params int[] indexes)
    {
        //print(indexes.JoinString());
        Vector3 _a = pts[indexes[0]];
        Vector3 _b = pts[indexes[1]];
        Vector3 _c = pts[indexes[2]];
        Vector3 _d = pts[indexes[3]];

        var dOrient = MathHelper.Orient(_a, _b, _c, _d);
        if (dOrient > 0)
        {
            //0 1 2 3
            //a b c d
            return new int[] { indexes[0], indexes[1], indexes[2] };
        }
        return new int[] { indexes[2], indexes[1], indexes[0] };
    }

    public double VolumeOfTetrahedara(Vector3[] pts, int4 tet)
    {
        Vector3 a = pts[tet[0]];
        Vector3 b = pts[tet[1]];
        Vector3 c = pts[tet[2]];
        Vector3 d = pts[tet[3]];

        var da = a - d;
        var db = b - d;
        var dc = c - d;

        return Math.Abs(MatrixMath.Determinant(new double[,] {
            { da.x, da.y, da.z },
            { db.x, db.y, db.z },
            { dc.x, dc.y, dc.z }
        })) / 6;
    }

    private bool autoInsert = false;
    private void Update()
    {
        enabled = false;

        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            FindInserts();
            InsertPoints();
        }
        if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            UpdateAdjacents();
        }
        if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            FindFlips();
            DoFlips();
        }
        if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            FindObjectOfType<GPUBuilderVisualizer>().RandomizeColors();
        }

        if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            autoInsert = !autoInsert;
        }

        if (autoInsert)
        {
            var pointsLeft = pointsToInsert.Count(x => x >= 0);
            Debug.LogFormat("Points left: {0}", pointsLeft);
            if (pointsLeft > 0)
            {
                FindInserts();
                InsertPoints();

                while (tetrahedrasToCheck.Length > 0)
                {
                    FindFlips();
                    DoFlips();
                }
            }
            else
            {
                DidFinishTriangulationEvent(GetVolume());
                Debug.Log("Tetrahedras: " + tetrahedrons.Count(x => x.a >= 4));
                autoInsert = false;
            }
        }

        enabled = true;
    }
}
