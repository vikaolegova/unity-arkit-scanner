﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public class GPUBuilderVisualizer : MonoBehaviour
{
    private DelaunayBuilderGPU builder;

    public Material material;
    private List<Tetrahedron> tetrahedrons = new List<Tetrahedron>();

    public bool expand;
    public float expandSize = 2;
    public float expandSpeed = 1;

    public Material hullMat;

    void Awake()
    {
        builder = FindObjectOfType<DelaunayBuilderGPU>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            RandomizeColors();
        }

        if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            GenerateHull();
        }

        //Expand();
    }

    public GameObject GenerateHull()
    {
        Debug.Assert(hullMat);

        var go = new GameObject("HULL");
        var filter = go.AddComponent<MeshFilter>();
        var rend = go.AddComponent<MeshRenderer>();
        go.transform.SetParent(transform.GetChild(0));
        go.transform.localPosition = Vector3.zero;
        go.transform.localRotation = Quaternion.identity;
        go.transform.localScale = Vector3.one;

        transform.GetChild(0).localScale = Vector3.one;

        filter.mesh = builder.ExportMeshFromExternalFacets();

        Material[] mats = new Material[filter.mesh.subMeshCount];
        for (int i = 0; i < mats.Length; i++)
        {
            mats[i] = new Material(hullMat)
            {
                color = UnityEngine.Random.ColorHSV()
            };
        }
        rend.materials = mats;

        return go;
    }

    public void RandomizeColors()
    {
        foreach (var t in tetrahedrons)
        {
            var mr = t.meshRenderer;
            if (mr) Destroy(mr.gameObject);
        }

        Tetrahedron.parentTransform = transform.GetChild(0);
        Tetrahedron.realPoints = builder.points.Select(x => (VisiblePoint)x).ToList();
        Tetrahedron.material = material;

        tetrahedrons.Clear();
        foreach (var t in builder.tetrahedrons)
        {
            tetrahedrons.Add(new Tetrahedron((int)t.a, (int)t.b, (int)t.c, (int)t.d));
        }

        foreach (var t in tetrahedrons)
        {
            t.GenerateMesh();

            var color = Random.ColorHSV();
            color.a = 1;

            if (t.Points.Any(x => x < 4))
                color.a = 0;
            t.Color = color;
            //t.meshRenderer.enabled = true;
        }
    }

    void Expand()
    {
        try
        {
            foreach (var t in tetrahedrons)
            {
                var pts = t.Points.Select(x => builder.points[x]).ToArray();
                var center = Vector3.zero;
                foreach (var p in pts)
                {
                    center += (Vector3)p / 4;
                }

                if (t.meshFilter)
                {
                    var targetPos = expand ? center * expandSize : Vector3.zero;

                    t.meshFilter.transform.position =
                        Vector3.Lerp(t.meshFilter.transform.position, targetPos, Time.deltaTime * expandSpeed);
                }
            }
        }
        catch { }
    }
}
