﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ModelTriangulator : MonoBehaviour
{
    public Mesh mesh;

    private ObjectScanManager scanManager;
    private ScanUIController scanUIController;

	private void Awake()
	{
        scanManager = FindObjectOfType<ObjectScanManager>();
        scanUIController = FindObjectOfType<ScanUIController>();
	}

	private void Start()
    {
        if (!mesh)
        {
            print("Mesh is not assigned");
            return;
        }
        scanManager.scannedPoints = mesh.vertices.ToList();
        scanUIController.ShowPointsCount();
    }
}
